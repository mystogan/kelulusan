
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/progressbar/css/cssprogress.css">
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title mb-4">Setting</h5>
                  <div class="fluid-container">
                    <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
                      <div class="col-md-1">
                        <i class="mdi mdi-poll-box text-success icon-lg"></i>
                      </div>
                      <div class="ticket-details col-md-4">
                        <div class="d-flex">
                          <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">Kelulusan Sistem</p>
                        </div>
                        <p class="text-gray ellipsis mb-2">Pengaturan untuk otomatis sistem akan mengatur Kelulusan</p>
                        
                      </div>
                      <div class="ticket-actions col-md-2">
                        <div class="btn-group dropdown">
                          <?php
                          $i = 0;
                          foreach ($choice as $value) { ?>
                          <!-- <a href="<?php echo base_url()."admin/autoChoice/".$value->cs_id;?>" class="btn btn-<?php echo $warna[$i];?> btn-sm" >Auto <?php echo $value->cs_name;?></a>&emsp;                             -->
                          <a href="javascript:autochoice('<?php echo $value->cs_id;?>');" class="btn btn-<?php echo $warna[$i];?> btn-sm" >Auto <?php echo $value->cs_name;?></a>&emsp;                            
                          <?php
                          $i++;
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
                      <div class="col-md-1">
                      <i class="mdi mdi-poll-box text-warning icon-lg"></i>
                      </div>
                      <div class="ticket-details col-md-4">
                        <div class="d-flex">
                          <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">Setting Tahapan</p>
                        </div>
                        <p class="text-gray ellipsis mb-2">Pengaturan untuk buka tahapan</p>
                      </div>
                      <div class="ticket-actions col-md-2">
                        <div class="btn-group dropdown">
                          <?php
                          $i = 0;
                          foreach ($choice as $value) { ?>
                          <button onclick="javascript:tutupkelulusan(0,'<?php echo $value->cs_id;?>','Buka <?php echo $value->cs_name;?>')" type="button" class="btn btn-<?php echo $warna[$i];?> btn-sm" >Buka <?php echo $value->cs_name;?></button>&emsp;                            
                          <?php
                          $i++;
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <button onclick="javascript:tutupkelulusan(0,'-1','Menutup Kelulusan');" type="button" class="btn btn-danger btn-sm" >
                      Tutup Kelulusan
                    </button>
                    <button onclick="javascript:tutupkelulusan(0,'-2','Afirmasi');" type="button" class="btn btn-default btn-sm" >
                      Afirmasi
                    </button>
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <button type="button" class="btn btn-default btn-sm" style="background-color:#FEC98D" ></button> Intervensi &ensp;|&ensp;
                    <button type="button" class="btn btn-default btn-sm" style="background-color:#E3E3E3" ></button> Tutup &ensp;|&ensp;
                    <button type="button" class="btn btn-default btn-sm" style="background-color:#2623D2" ></button> Auto &ensp;|&ensp;
                    <button type="button" class="btn btn-default btn-sm" style="background-color:#CEFECE" ></button> Tahapan
                  </div>
                  <!-- <br>
                  <div id="myProgress">
                    <div id="myBar"></div>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4>Tahapan Kelulusan <?php echo date("Y");?></h4>
                  <div class="loading">
                    <img src="<?php echo base_url();?>assets/img/spin.gif" alt="" style="width:20px;">
                  </div>
                    <div class="table-responsive" id="tahapan"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>

<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/tahapan.js"></script>

<style>
.table th, .table td {
  padding: 10px 10px;
  vertical-align: top;
  border-top: 1px solid #f2f2f2;
}
#myProgress {
  width: 100%;
  background-color: #ddd;
}

#myBar {
  width: 0%;
  height: 30px;
  background-color: #4CAF50;
}
</style>