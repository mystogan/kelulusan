
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4>List Verifikator</h4>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                              <th>Nama Perguruan Tinggi</th>
                              <th>Nama Verifikator</th>
                              <th>NIP</th>
                              <th>Jabatan</th>
                              <th>Telepon</th>
                              <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach ($verifikator as $value) { ?>
                            <tr>
                              <td><?php echo $value->coll_name;?></td>
                              <td><?php echo $value->veri_name;?></td>
                              <td><?php echo $value->veri_nip;?></td>
                              <td><?php echo $value->pos_name;?></td>
                              <td><?php echo $value->veri_hp;?></td>
                              <td><?php echo $value->veri_email;?></td>
                            </tr>
                            
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>

      <script>
      $(document).ready(function() {
          $('#idDatatable').DataTable();
      } );

      </script>
      