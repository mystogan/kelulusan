

<script type="text/javascript" src="http://ods.uinsby.ac.id/assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="http://ods.uinsby.ac.id/assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto; 
        margin: 0mm;
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Cetak Kelulusan</title></head>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto;margin-top:20px;">
	<tr>
		<td>


	<div id="header">
			<div id="gambar"><img style=" width:80%;" src="<?php echo base_url();?>assets/img/kemenag.png"/></div>
      <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA
        <br/>DIREKTORAT KURIKULUM SARANA KELEMBAGAAN DAN KESISWAAN MADRASAH</strong>
	  <br/><span style="font-size:0.85em">    <strong>SELEKSI NASIONAL PESERTA DIDIK BARU TAHUN <?php echo date("Y");?></strong></span>
          
      <p style=" margin-top: 10px;font-size:0.6em">
      ® copyright 2018 Kementerian Agama - UIN Sunan Ampel Surabaya <br>
            Website: madrasah.kemenag.go.id
    </p></div>
		</div>

		<div id="garis"></div>
    <br/>
    <br/>
	<div id="satu">
    <table width="680px;" style="margin:auto; font-size:0.9em;">
      <tr>
        <td>
          <div style="text-align:center;">
            <strong > DAFTAR LULUSAN <br>SNPDB MAN-IC, MAN-PK, MAKN Tahun <?php echo date("Y");?> <br></strong>
          </div>
          <br/>

          <table border="1" id="customers" style="border-collapse: collapse; font-size: 0.9em; width:100%;" >
          <tr>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NO</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NOMOR PESERTA</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NAMA LENGKAP</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >JENIS KELAMIN</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >MADRASAH</th>
          </tr>
          <?php
          $i = 1;
          foreach ($student as $value) { ?>
          <tr>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $i++;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $value->std_code;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:left;" ><?php echo $value->std_name;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php 
            if ($value->std_gender == 0) {
              echo "Laki Laki";
            }else {
              echo "Perempuan";
            }
            ?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $value->mjr_name;?></td>
          </tr>
            
          <?php
          }
          ?>
        </table>
          
          <div></div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <div id="id">
            <strong>
              Purwokerto, 24 April 2019              <br/>Kepala Madrasah            </strong>

          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <strong>
            (____________________________)<br/>
            NIP. 
          </strong>

          </div></td>
          </table>
		  </div>
        </td>
      </tr>
	</tr>
</table>



	</body>
</font>
</html>