
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4>Rekapitulasi Kelulusan <?php echo date("Y");?></h4>
                  <div class="loading">
                    <img src="<?php echo base_url();?>assets/img/spin.gif" alt="" style="width:20px;">
                  </div>
                    <div class="table-responsive" id="rekapitulasi">
                      
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      <script>
      $(document).ready(function() {
          $('#idDatatable').DataTable();
      } );

      </script>
      
      <script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/rekapitulasi.js"></script>

<style>
.table th, .table td {
  padding: 18px 10px;
  vertical-align: top;
  border-top: 1px solid #f2f2f2;
}
</style>