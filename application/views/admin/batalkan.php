
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Batalkan Peserta </h4>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Masukkan Kode Siswa</label>
                          <div class="col-sm-9">
                          <input type="text" class="col-md-6 form-control" placeholder="Masukkan Kode Siswa" id="id_search">
                          </div>
                        </div>
                      </div>
                    <label class="col-sm-12 col-form-label" style="color:<?php echo $_color; ?>;">
                    <?php echo $_err?>
                    </label>
                    <label class="col-sm-12 col-form-label" style="color:<?php echo $_color; ?>;">
                      <?php echo $_err2?>
                    </label>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<!-- start modal  -->
<div class="modal fade" id="viewSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document" style="max-width:1000px;">
            <div class="modal-content" style="background-color:#fff;">
              <div class="modal-body" id="detailsiswa">
                
              </div>
              <div class="modal-footer">
                <a id="batalin" href="" class="btn btn-danger" >Batalkan</a>
                <a  class="btn btn-default" data-dismiss="modal">Close</a>
              </div>
            </div>
          </div>
        </div>
        <!-- end modal -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>

// search dan muncul modal untuk selain afirmatif
$("#id_search").keyup(function(event) {
    if (event.keyCode === 13) {
      let std_code = $("#id_search").val();
      std_code = std_code.trim()
      $.ajax({
        url : base_url+"admin/checkstudent/"+std_code, 
        type: "post", 
        success:function(response){    
          let data = JSON.parse(response)
          if(data.cek == 0){
            alert (data.pesan)
          }else {
            $('#detailsiswa').load(base_url+"panlok/getDetailSiswa/"+data.std_id);
            $("#viewSiswa").modal('show')
            $("#batalin").attr("href", "javascript:confirBatal('"+std_code+"')")
          }
        },
        error: function(xhr, Status, err) {
          console.log("Terjadi error : "+Status)
        }
        
      });
    }
});

function confirBatal(kode){
  if (confirm('Apakah Anda akan membatalkan kelulusan siswa yang berkode '+kode+" ?")) {
    window.location.assign("<?php echo base_url()?>admin/siswaBatal/"+kode)
  } 
}


</script>
