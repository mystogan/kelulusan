<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title> Aplikasi Kelulusan</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/css/vendor.bundle.addons.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/style.css">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/lulus.png" />
	<script>
	var base_url = '<?php echo base_url()?>';
	</script>
	<script src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
	
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="<?php echo base_url();?>assets/admin/images/logo.svg" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="<?php echo base_url();?>assets/admin/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            
          </li>
          <li class="nav-item active">
          </li>
          <li class="nav-item">
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            
          </li>
          <li class="nav-item dropdown">
            
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text"> <?php echo $this->session->userdata('name_college');?> </span>
              <img class="img-xs rounded-circle" src="<?php echo base_url();?>assets/img/<?php echo $this->session->userdata('coll_foto');?>" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <?php
              if ($this->session->userdata('id_login') != '0') { ?>
              <a class="dropdown-item" data-toggle="modal" data-target="#changePassword">
                Change Password
              </a>
                
              <?php
              }
              ?>
              <a class="dropdown-item" href="<?php echo base_url();?>home/logout">
                  Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <button class="btn btn-success btn-block">
                <?php   
              $month = $this->session->userdata('month');
              echo date('d')." ".$month[(date('m')-1)]." ".date('Y');?>

              </button>
            </div>
          </li>
          
          <?php
          if($this->session->userdata('id_login') == 0){ ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Rekap</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link " href="<?php echo base_url();?>admin/rekapMax" >Nilai tertinggi</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url();?>admin/perprov">Per Provinsi</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url();?>admin/rekaplulus">Kelulusan</a>
                </li>
              </ul>
            </div>
          </li>
		      <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-sticker"></i>
              <span class="menu-title">Kelulusan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>admin/rekapitulasi">Rekapitulasi</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>admin/tahapan">Tahapan</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>admin/verifikator">Verifikator</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>admin/batalkan">Batalkan</a>
                </li>
              </ul>
            </div>
          </li>
		      <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#import" aria-expanded="false" aria-controls="import">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">Import</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="import">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>import/mahasiswa">Mahasiswa</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>import/matpel">Mata Pelajaran</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>import/majoring">Nilai Majoring</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>import/choice">Pilihan</a>
                </li>
              </ul>
            </div>
          </li>
          <?php
          }else { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>panlok/resume">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Panlok</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url();?>panlok/profil">Profil</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url();?>panlok/prodiPTN">Program Studi</a>
                </li>
              </ul>
            </div>
          </li>
		      <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#seleksi" aria-expanded="false" aria-controls="seleksi">
              <i class="menu-icon mdi mdi-sticker"></i>
              <span class="menu-title">Seleksi</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="seleksi">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>panlok/seleksi">Seleksi</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>panlok/calonlulusan">Daftar Calon Lulusan</a>
                </li>
              </ul>
            </div>
          </li>
		      <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">Kelulusan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url();?>panlok/pengesahan">Pengesahan</a>
                </li>
              </ul>
            </div>
          </li>
          <?php
          }
          ?>
		      

		      

        </ul>
      </nav>
      <!-- partial -->


      