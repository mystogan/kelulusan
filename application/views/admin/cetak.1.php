

<html>

	<head><title>Cetak</title></head>
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/cetak.css" type="text/css"/> -->
  <font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto;max-width: 650px;">
	<tr>
		<td>
        <div id="header">
            <div id="gambar"><img style="height:65%; " src="<?php echo base_url();?>assets/img/kemenag.png"/></div>
            <div id="tulisan_uin" style="font-size:1.2em"><strong>KEMENTERIAN AGAMA</strong><br/>
            <span style="font-size:0.7em">
              <strong>UJIAN MASUK PERGURUAN TINGGI KEAGAMAAN ISLAM NEGERI TAHUN <?php echo date("Y");?></strong></span>
            <p style=" margin-top: 10px;font-size:0.6em">
            ® copyright 2017 Kementerian Agama - Perguruan Tinggi Agama Islam Negeri <br>
            Website: www.um-ptkin.ac.id; e-mail: info@um-ptkin.ac.id
          </p></div>
          </div>

          <div id="garis"></div>
		  
	<div id="satu">
        <table style="font-size:0.9em;">
        </table>
        </br>
        <div style="line-height:20px; text-align: center;">
          <strong > DAFTAR LULUSAN <br>UM PTKIN <?php echo date("Y");?> <br>ADMINISTRATOR </strong>
        </div>
        </br>
        <table border="1" id="customers" style="border-collapse: collapse; font-size: 0.9em">
          <tr>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NO</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NOMOR PESERTA</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NAMA LENGKAP</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >JENIS KELAMIN</th>
            <th style="border: 1px solid #000;  padding: 8px; text-align:center;" >PROGRAM STUDI</th>
          </tr>
          <?php
          $i = 1;
          foreach ($student as $value) { ?>
          <tr>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $i++;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $value->std_code;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:left;" ><?php echo $value->std_name;?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php 
            if ($value->std_gender == 0) {
              echo "Laki Laki";
            }else {
              echo "Perempuan";
            }
            ?></td>
            <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $value->mjr_name;?></td>
          </tr>
            
          <?php
          }
          ?>
        </table>
        <div>
          <div>
						<div id="id1" style="font-size:0.9em;">
						  Surabaya, <?php echo date('d')." ".date('m')." ".date('Y');?>
              <br/>Mengetahui,<br/><?php echo $cetak->jabatan." ".date('Y');?>
              <br/>
							<br/>
							<br/>
							<br/>
							<br/>
							<strong><?php echo $cetak->nama;?><br/></strong>
							NIP. <?php echo $cetak->nip;?>						

						</div>
					</div>
					    		</div>
		</div>
	</td>
	</tr>
</table>

	</body>
</font>
</html>