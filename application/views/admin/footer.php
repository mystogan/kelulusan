      <!-- start modal  -->
      <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="max-width:500px;">
          <div class="modal-content" style="background-color:#fff;">
            <div class="modal-body" id="detailsiswa">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Ganti Password</h4>
                      <form class="form-sample" id="gantiPass" method="POST">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Password Lama</label>
                              <div class="col-sm-9">
                                <input type="password" class="form-control" name="old_password" id="old_password" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Password Baru</label>
                              <div class="col-sm-9">
                                <input type="password" class="form-control" name="new_password" id="new_password" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Konfirmasi Password</label>
                              <div class="col-sm-9">
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label"></label>
                              <div class="col-sm-9">
                                <button type="button" class="btn btn-success" id="btnSimpan" >Simpan</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      <h6 style="color:red;" id="textpesan"></h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal -->
      <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="" style="display:none;">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- bagian.e footer -->
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url();?>assets/admin/vendors/js/vendor.bundle.addons.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url();?>assets/admin/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>