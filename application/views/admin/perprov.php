
      <div class="main-panel">
        <div class="content-wrapper">
		<div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-cube text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Total Peserta</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $allStud->hasil;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Jumlah Peserta yang mengikuti Tes
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-receipt text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Lulusan</p>
                      <div class="fluid-container">
                      <h3 class="font-weight-medium text-right mb-0"><?php 
                      if($allgraduate->hasil == ''){
                        echo '0';
                      }else {
                        echo $allgraduate->hasil;
                      }
                      ?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Jumlah Peserta yang lulus
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-poll-box text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Jumlah Sekolah</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $allSchool->hasil;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Jumlah Sekolah yang mengikuti Tes
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-account-location text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Jumlah Provinsi</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $allProv->hasil;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Jumlah Provinsi yang mengikuti Tes
                  </p>
                </div>
              </div>
            </div>
          </div>
		  <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <h4>Nilai Tertinggi Per Provinsi</h4>
                  <!-- <div class="loading">
                    <img src="<?php echo base_url();?>assets/img/spin.gif" alt="" style="width:20px;">
                  </div> -->
                  <div class="table-responsive">
                    <table class="table table-striped" id="rekapProv">
                      <tr>
                        <th>Nama Provinsi</th>
                        <?php
                        foreach ($matpel as $value) {
                          echo '<th>'.$value->mpl_name.'</th>';
                        }
                        ?>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        
        </div>

<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/app.js"></script>