
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Upload Mahasiswa</h4>
                  <form class="form-sample" method="POST" action="<?php echo base_url();?>import/importMhs" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">File Upload</label>
                          <div class="col-sm-9">
                            <input type="file" name="mahasiswa" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <button class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                    <label class="col-sm-12 col-form-label" style="color:<?php echo $_color; ?>;">
                      <?php echo $_err?>
                    </label>
                    <label class="col-sm-12 col-form-label" style="color:<?php echo $_color; ?>;">
                      <?php echo $_err2?>
                    </label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Klik Template <a href="<?php echo base_url();?>assets/template/template-mahasiswa.csv">disini</a></label>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      <script>
      $(document).ready(function() {
          $('#idDatatable').DataTable();
      } );

      </script>

