<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login Aplikasi Kelulusan</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/lulus.png" />
  <!-- plugins:css -->
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/css/vendor.bundle.addons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/style.css">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
			<form class="form-horizontal" action="<?php echo base_url();?>home/ceklogin" method="post">
			  	<input type="hidden" name="login" value="login"/>
				<input type="hidden" value="<?php echo $this->session->userdata('_key');?>" name="_key">
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
				  <select class="form-control" name="username" id="select2-single-box" name="select2-single-box" data-placeholder="Pick your choice" data-tabindex="1">
						<?php
						foreach ($college as $value) { ?>
						<option value="<?php echo $value->coll_code; ?>"><?php echo $value->coll_name; ?></option>								
						<?php
						}
						?>
					</select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" name="password" placeholder="*********">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
				<span style="color:red;"><?php echo $_err;?></span>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Login</button>
                </div>
              </form>
            </div>
            <p class="footer-text text-center" style="display:none;">copyright © 2018 Bootstrapdash. All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/login.js"></script>

</body>

</html>