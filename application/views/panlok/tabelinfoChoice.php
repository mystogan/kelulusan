<?php 
                  
                  $o = 0;
                  $sum = 0;
                  $graduate = 0;
                  $fail = 0;
                  $j=1;
                  foreach ($choice as $value) {
                    $pilih[$j++] = $value->cs_name;
                    if(count($choice)== 2){
                      $hitung = 6;
                      $icons1 = '<div class=""><i class="mdi mdi-receipt text-warning icon-lg"></i></div>';
                      $icons2 = '<div class=""><i class="mdi mdi-receipt text-info icon-lg"></i></div>';
                      $icons3 = '<div class=""><i class="mdi mdi-receipt text-danger icon-lg"></i></div>';
                    }else if(count($choice)== 3){
                      $hitung = 4;
                      $icons1 = '<div class=""><i class="mdi mdi-receipt text-warning icon-lg"></i></div>';
                      $icons2 = '<div class=""><i class="mdi mdi-receipt text-info icon-lg"></i></div>';
                      $icons3 = '<div class=""><i class="mdi mdi-receipt text-danger icon-lg"></i></div>';
                    }else if(count($choice)== 4){
                      $hitung = 3;
                      $icons1 = '';
                      $icons2 = '';
                      $icons3 = '';
                    }else {
                      $hitung = 2;
                      $icons1 = '';
                      $icons2 = '';
                      $icons3 = '';
                    }
                  ?>
                    <div class="col-md-<?php echo $hitung;?> d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <div class="card-body">
                          <h4 class="card-title" style="margin-bottom:-1.4em;"><?php echo $value->cs_name;?></h4> 
                        <div class="row pt-3">
                          <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                          <?php echo $icons1;?>
                            <div class="wrapper d-flex flex-column justify-content-center">
                              <small class="font-weight-medium text-gray">Jumlah</small>
                              <h4 class="font-weight-bold mb-0"><?php 
                              if($sumchoice[$o]->hasil == ''){
                                $sumchoice[$o]->hasil = '0';
                              }
                              echo $sumchoice[$o]->hasil;
                              ?></h4>
                            </div>
                          </div>
                          <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                          <?php echo $icons2;?>
                            <div class="wrapper d-flex flex-column justify-content-center">
                              <small class="font-weight-medium text-gray">Lulus</small>
                              <h4 class="font-weight-bold mb-0"><?php 
                              if($sumgraduate[$o]->hasil == ''){
                                $sumgraduate[$o]->hasil = '0';
                              }
                              echo $sumgraduate[$o]->hasil;
                              ?></h4>
                            </div>
                          </div>
                          <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row">
                          <?php echo $icons3;?>
                            <div class="wrapper d-flex flex-column justify-content-center">
                              <small class="font-weight-medium text-gray">Gagal</small>
                              <h4 class="font-weight-bold mb-0"><?php 
                              echo $sumchoice[$o]->hasil-$sumgraduate[$o]->hasil ;?></h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                    $sum = $sum+$sumchoice[$o]->hasil;
                    $graduate = $graduate+$sumgraduate[$o]->hasil;
                    $fail = $fail+($sumchoice[$o]->hasil-$sumgraduate[$o]->hasil);
                    $o++;
                }
                  ?>
                  </div>
                  <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0" style="margin-top:-1.4em;">
                    <div class="card-body">
                        <h4 class="card-title" style="margin-bottom:-1.4em;">Jumlah</h4> 
                      <div class="row pt-3">
                        <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                        <?php echo $icons1;?>
                          <div class="wrapper d-flex flex-column justify-content-center">
                            <small class="font-weight-medium text-gray">Jumlah</small>
                            <h4 class="font-weight-bold mb-0"><?php 
                            echo $sum ;?></h4>
                          </div>
                        </div>
                        <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                        <?php echo $icons2;?>
                          <div class="wrapper d-flex flex-column justify-content-center">
                            <small class="font-weight-medium text-gray">Lulus</small>
                            <h4 class="font-weight-bold mb-0"><?php 
                            echo $graduate ;?></h4>
                          </div>
                        </div>
                        <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row">
                        <?php echo $icons3;?>
                          <div class="wrapper d-flex flex-column justify-content-center">
                            <small class="font-weight-medium text-gray">Gagal</small>
                            <h4 class="font-weight-bold mb-0"><?php 
                            echo $fail ;?></h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0" style="margin-top:-1.4em;">
                    <div class="card-body">
                      <div class="row pt-12">
                        <div class="col-md-12 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                          <h3 class="text-primary">Status : (
                          <?php 
                          $ceke = 0;
                          for ($i=1; $i <= count($pilih); $i++) { 
                            if($college->coll_state == $i){
                              $status = 'Tahap '.$i;
                              $ceke = 1;
                              break;
                            }
                          }
                          if ($ceke == 0) {
                            if($college->coll_state == '-2'){
                              $status = 'Afirmasi';
                            }else if($college->coll_state == '0'){
                              $status = 'Auto Sistem';
                            }else {
                              $status = 'Sudah Ditutup';
                            }
                          }
                          print_r ($status);?> )</h3> 
                        </div>
                      </div>
                    </div>