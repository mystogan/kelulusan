
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card ">
                <div class="card-body">
                  <div class="row pt-3" style="margin-bottom:-2em;">
                    <div class="col-md-8 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <h2 class="text-primary mb-5"><?php echo $this->session->userdata('name_college');?></h2>
                    </div>
                    <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <h4 class="col-md-5 text-primary" >Kode Siswa </h4>
                      <?php
                      // kondisi dimana untuk menentukan modal jika afirmasi maka modal yang dimunculkan ada inputnyan
                      if($college->coll_state != '-2'){
                        echo '<input type="text" class="col-md-6 form-control" placeholder="Masukkan Kode Siswa" id="id_search">';
                      }else {
                        echo '<input type="text" class="col-md-6 form-control" placeholder="Masukkan Kode Siswa" id="id_search_afirmatif">';
                      }
                      ?>
                      
                    </div>
                  </div>
                  <div class="row pt-3">
                    <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <select name="mjr_code" id="mjr_code" class="form-control" onchange="javascript:loadTable();">
                        <?php
                        foreach ($majoring as $value) { ?>
                          <option value="<?php echo $value->mjr_code;?>"><?php echo $value->mjr_name;?></option>                      
                        <?php
                        }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-2 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <select name="cs_id" id="cs_id" class="form-control" onchange="javascript:loadTable();" disabled>
                        <option value="0" <?php if($college->coll_state <= 0){ echo "selected"; }?> >All</option>
                        <?php
                        foreach ($choice as $value) { ?>
                        <option value="<?php echo $value->cs_id;?>" <?php if($college->coll_state == $value->cs_id){ echo "selected"; }?> ><?php echo $value->cs_name;?></option>
                        <?php
                        }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-2 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                    
                    </div>
                    <div class=""><i class="mdi mdi-cube text-default icon-lg"></i></div>
                    <div class="col-md-2 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0" id="kuotaprodi">
                      Kuota  
                    </div>
                  </div>
                  <div class="row pt-3" id="infoChoice">
                  
                  </div>
                  <br>
                  <div id="loadTable" style="margin-top:-7em;">
                  
                  </div>
                  
                </div>
              </div> 
            </div>
          </div>
        </div>
        
        <!-- start modal  -->
        <div class="modal fade" id="viewSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document" style="max-width:1000px;">
            <div class="modal-content" style="background-color:#fff;">
              <div class="modal-body" id="detailsiswa">
                
              </div>
              <div class="modal-footer">
                <a  class="btn btn-default" data-dismiss="modal">Close</a>
              </div>
            </div>
          </div>
        </div>
        <!-- end modal -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/seleksi.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
        <script src="<?php echo base_url();?>assets/admin/js/login.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
