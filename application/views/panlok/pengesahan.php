
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card ">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-5">Pengesahan <?php echo $this->session->userdata('name_college');?></h2>
                  <?php
                  if($college->coll_confirm == 0){ ?>
                  <a href="<?php echo base_url();?>panlok/pengesahanpanlok" class="btn btn-danger">Klik untuk Pengesahan</a>
                  <?php
                  }
                  ?>
                  <table class="table table-striped">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Prodi</th>
                          <th>Nama Prodi</th>
                          <th> <center>Kuota <br> Awal</center> </th>
                          <th> <center>Kuota<br> Perubahan</center> </th>
                          <th> <center>Kuota<br> Terisi</center> </th>
                          <th> <center>Detail</center> </th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i = 1;
                      foreach ($pengesahan as $value) { ?>
                        <tr>
                          <td><?php echo $i++?></td>
                          <td><?php echo $value->mjr_code?></td>
                          <td><?php 
                          $mjr_name = explode(' ',$value->mjr_name);
                          for ($j=0; $j < count($mjr_name); $j++) { 
                            if($j%3 == 0){
                              echo '<br/>';
                            }
                            echo $mjr_name[$j]." ";
                          }
                          ?></td>
                          <td> <center><?php echo $value->mjr_quota;?></center> </td>
                          <td> <center><?php echo $value->mjr_quota_change;?></center> </td>
                          <td> <center><?php echo $value->hasil;?></center> </td>
                          <td> <center><a class="btn btn-primary" href="javascript:ceksiswa('<?php echo $value->mjr_code?>');">Lihat Siswa</a></center> </td>
                        </tr>
                        
                      <?php
                      }
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>


        <!-- start modal  -->
        <div class="modal fade" id="viewSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document" style="max-width:1000px;">
            <div class="modal-content" style="background-color:#fff;">
              <div class="modal-body">
                <table style="" class="table table-striped" id="cekpeserta"></table>  
              </div>
              <div class="modal-footer">
                <a  class="btn btn-default" data-dismiss="modal">Close</a>
              </div>
            </div>
          </div>
        </div>
        <!-- end modal -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script>
function cekkuota(noid)
{
  let value = $('#'+noid).val();
  $.ajax({
        url: base_url+"panlok/cekkuotaubah",
        type: "post",
        data:  'mjr_id='+noid+'&value='+value,
        success: function (response) {
          let data = JSON.parse(response)
          if(data.cek == 0){
            $('#'+noid).val(data.kuota)
          }
          alert (data.pesan)
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
    });
}
function ceksiswa(mjr_code){
  $("#viewSiswa").modal('show')
  $('#cekpeserta').load(base_url+"panlok/getMajorGraduate/"+mjr_code);

}
</script>