<div class="container"><h4>Detail Siswa (Afirmasi)</h4></div>
  <div id="exTab2" class="container">	
    <ul class="nav nav-tabs">
      <li class="active">
        <a  href="#1" data-toggle="tab">Data Diri</a>
      </li>
      <li><a href="#2" data-toggle="tab">Nilai Siswa</a>
      </li>
      <li><a href="#3" data-toggle="tab">Pilihan</a>
      </li>
      <li><a href="#4" data-toggle="tab"><?php echo ucwords($student->std_personality);?></a>
      </li>
    </ul>
 
      <div class="tab-content ">
        <div class="tab-pane active" id="1">
          <div class="row pt-3">
            <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
              <div class="wrapper d-flex flex-column justify-content-center">
                <img src="<?php echo base_url();?>assets/img/<?php echo $student->std_foto;?>" alt=""> 
              </div>
            </div>
            <div class="col-md-8 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
              <div class="wrapper d-flex flex-column justify-content-center">
                <table class="table table-striped">
                  <tr>
                    <td>Nomor Peserta Ujian</td>
                    <td>:</td>
                    <td><?php echo $student->std_code;?></td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    <td>:</td>
                    <td><?php echo $student->std_name;?></td>                    
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?php 
                    if($student->std_gender == '1'){
                      echo "Perempuan";
                    }else {
                      echo "Laki-Laki";
                    }
                    ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php echo $student->std_bdate;?></td>                    
                  </tr>
                  <tr>
                    <td>Telepon / Ponsel</td>
                    <td>:</td>
                    <td><?php echo $student->std_phone;?></td>
                  </tr>
                  <tr>
                    <td>Alamat / Kediaman</td>
                    <td>:</td>
                    <td><?php 
                    $alamat = explode(' ',$student->std_address);
                    for ($i=0; $i < count($alamat); $i++) { 
                      if($i%6 == 0){
                        echo '<br/>';
                      }
                      echo $alamat[$i]." ";
                    }
                    ?></td>
                  </tr>
                  <tr>
                    <td>Kabupaten / Kota</td>
                    <td>:</td>
                    <td><?php echo $student->std_regency;?></td>
                  </tr>
                  <tr>
                    <td>Propinsi</td>
                    <td>:</td>
                    <td><?php echo $student->std_province;?></td>
                  </tr>
                  <?php
                  if ($majoring->coll_id == $this->session->userdata('log_user_id') || $this->session->userdata('log_user_id') == 1) { ?>
                  <tr>
                    <td>Diterima di :</td>
                    <td>:</td>
                    <td><?php echo $majoring->mjr_name."<br> (".$majoring->coll_name.")";?> <br>
                    Pilihan ke : <?php echo substr($student->std_pilihan,2,1);?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                <?php
                
                if(substr($student->std_pilihan,0,1) != 1){
                  foreach ($choice as $value) { 
                    if ($value->coll_id == $this->session->userdata('log_user_id')) { ?>
                      <label class="checkbox-inline">
                        <input type="radio"  name="choice_afirmasi" id="afir<?php echo $value->mjr_code;?>" onclick="javascript:ceksiswa_afir('<?php echo $student->std_id;?>','<?php echo $value->mjr_code;?>')" value="<?php echo $value->mjr_code;?>" <?php if($value->mjr_code == $student->std_lulus){echo "checked";}?> >
                        &emsp;<?php echo $value->cs_name." (".$value->mjr_name.")";?></label>
                  <?php
                    }
                  }
                }
                
                ?>

              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="2">
          <div class="row">
            <?php
            foreach ($matpel as $value) { 
            if(count($matpel) == 1){
              $hitung = 12;
            }else if(count($matpel) == 2){
              $hitung = 6;
            }else if(count($matpel) == 3){
              $hitung = 4;
            }else if(count($matpel) == 4){
              $hitung = 3;
            }else {
              $hitung = 2;
            }
            ?>
            <div class="col-xl-<?php echo $hitung;?> col-lg-<?php echo $hitung;?> col-md-<?php echo $hitung;?> col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-poll-box text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right"><?php echo $value->mpl_name;?></p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $value->sml_value;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i>Nilai <?php echo $value->mpl_name;?> yang didapat
                  </p>
                </div>
              </div>
            </div>
            <?php
            }
            ?>
            
            
          </div>
        </div>
        <div class="tab-pane" id="3">
          <div class="row">
            <?php
            foreach ($choice as $value) { 
            if(count($choice) == 1){
              $hitung = 12;
            }else if(count($choice) == 2){
              $hitung = 6;
            }else if(count($choice) == 3){
              $hitung = 4;
            }else if(count($choice) == 4){
              $hitung = 3;
            }else {
              $hitung = 2;
            }
            if ($value->coll_id == $this->session->userdata('log_user_id') || $this->session->userdata('log_user_id') == 1) {
              $display = '';
            }else {
              $display = 'style="display:none;"';
            }
            ?>
            <div <?php echo $display;?> class="col-xl-<?php echo $hitung;?> col-lg-<?php echo $hitung;?> col-md-<?php echo $hitung;?> col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-account-location text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right"><?php echo $value->cs_name;?></p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $value->mjr_name."<br/>(".$value->coll_name.")";?></h3>
                      </div>
                    </div>
                  </div>
                  <!-- <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i>Nilai <?php echo $value->mpl_name;?> yang didapat
                  </p> -->
                </div>
              </div>
            </div>
            <?php
            }
            ?>
            
            
          </div>
        </div>
        <div class="tab-pane" id="4">
        <br>
          <div class="row">
          <div class="col-md-12 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
              <div class="wrapper d-flex flex-column justify-content-center">
                <table class="table table-striped">
                  <tr>
                    <td>Nomor Peserta Ujian</td>
                    <td>:</td>
                    <td><?php echo $student->std_code;?></td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    <td>:</td>
                    <td><?php echo $student->std_name;?></td>                    
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?php 
                    if($student->std_gender == '1'){
                      echo "Perempuan";
                    }else {
                      echo "Laki-Laki";
                    }
                    ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php echo $student->std_bdate;?></td>                    
                  </tr>
                  <tr>
                    <td>Telepon / Ponsel</td>
                    <td>:</td>
                    <td><?php echo $student->std_phone;?></td>
                  </tr>
                  <tr>
                    <td>Alamat / Kediaman</td>
                    <td>:</td>
                    <td><?php 
                    $alamat = explode(' ',$student->std_address);
                    for ($i=0; $i < count($alamat); $i++) { 
                      if($i%6 == 0){
                        echo '<br/>';
                      }
                      echo $alamat[$i]." ";
                    }
                    ?></td>
                  </tr>
                  <tr>
                    <td>Kabupaten / Kota</td>
                    <td>:</td>
                    <td><?php echo $student->std_regency;?></td>
                  </tr>
                  <tr>
                    <td>Propinsi</td>
                    <td>:</td>
                    <td><?php echo $student->std_province;?></td>
                  </tr>
                </table>

              </div>
            </div>
          </div>
        </div>
      </div>
  </div>