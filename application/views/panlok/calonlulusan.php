
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card ">
                <div class="card-body">
                  <div class="row pt-3" style="margin-bottom:-2em;">
                    <div class="col-md-8 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <h2 class="text-primary mb-5">Daftar Calon Lulusan <?php echo $this->session->userdata('name_college');?></h2>
                    </div>
                  </div>
                  <div class="row pt-3">
                    <div class="col-md-4 d-flex align-items-center flex-md-column flex-lg-row mb-4 mb-md-0">
                      <select name="mjr_code" id="mjr_code" class="form-control" onchange="javascript:loadTable();">
                        <?php
                        foreach ($majoring as $value) { ?>
                          <option value="<?php echo $value->mjr_code;?>"><?php echo $value->mjr_name;?></option>                      
                        <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div id="loadTable" style="margin-top:-7em;">
                  
                  </div>
                  
                </div>
              </div> 
            </div>
          </div>
        </div>
        
        <!-- start modal  -->
        <div class="modal fade" id="viewSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document" style="max-width:1000px;">
            <div class="modal-content" style="background-color:#fff;">
              <div class="modal-body" id="detailsiswa">
                
                </div>
              <div class="modal-footer">
                <a  class="btn btn-default" data-dismiss="modal">Close</a>
              </div>
            </div>
          </div>
        </div>
        <!-- end modal -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  loadTable()
});
function loadTable()
{
  let mjr_code = $('#mjr_code').val();
  let cs_id = $('#cs_id').val();
  $('#loadTable').load(base_url+"panlok/getTblcalonlulusan/"+mjr_code+"/0");
}

</script>