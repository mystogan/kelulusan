
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-cube text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Kuota Awal</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $kuota->quota;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Kuota pada per-Institusi
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-receipt text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Kuota Perubahan</p>
                      <div class="fluid-container">
                      <h3 class="font-weight-medium text-right mb-0"><?php echo $kuota->quota_change; ?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Ada Penambahan Kuota
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-poll-box text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Total Peserta</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $sumStud->hasil;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Total Peserta yang mendaftar
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-account-location text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Total Lulusan</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"><?php echo $graduate->hasil;?></h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-reload mr-1" aria-hidden="true"></i>Siswa yang lulus
                  </p>
                </div>
              </div>
            </div>
          </div>
		      
          <div class="row">
            <div class="col-lg-7 grid-margin stretch-card">
              <!--weather card-->
              <div class="card ">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-5">Daftar 10 Peringkat Tertinggi</h2>
                  <table class="table table-striped">
                    <thead>
                        <tr>
                          <th>Nomor Pendaftaran</th>
                          <th>Nama Lengkap</th>
                          <th>Skor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($maxStud as $value) { ?>
                        <tr>
                          <td><?php echo $value->std_code;?></td>
                          <td><?php echo $value->std_name;?></td>
                          <td><?php 
                          $hasil =($value->jumlah); 
                          echo number_format($hasil,2,",","");
                          ?></td>
                        </tr>
                          
                        <?php
                        }
                        ?>

                    </tbody>
                  </table>
                </div>
              </div>
              <!--weather card ends-->
            </div>
            <div class="col-lg-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-5">Jumlah Pilihan </h2>
                  <?php
                  foreach ($choice as $value) { ?>
                  <div class="wrapper d-flex justify-content-between">
                    <div class="side-left">
                      <p class="mb-2">Pilihan <?php echo $value->cs_id;?> </p>
                      <p class="display-3 mb-4 font-weight-light"><?php echo $value->hasil;?> Orang</p>
                    </div>
                    <div class="side-right">
                      <small class="text-muted"><?php echo date('Y');?></small>
                    </div>
                  </div>

                  <?php
                  }
                  ?>

                </div>
              </div>
            </div>
          </div>
        </div>

