
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
          <div class="col-12 grid-margin">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Profil <?php echo $this->session->userdata('name_college');?></h4>
                <form name="verifier" action="<?php echo base_url();?>panlok/addVerifikator" method="post" class="form-horizontal">
                  <input type="hidden" name="veri_id" value="<?php echo $college->veri_id; ?>">
                  <input type="hidden" name="coll_id" value="<?php echo $this->session->userdata('log_user_id') ;?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Kode Panlok</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $this->session->userdata('log_user_id');?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama Verifikator</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="veri_name"  value="<?php echo $college->veri_name;?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama Panlok</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $college->coll_name;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email Verifikator</label>
                        <div class="col-sm-9">
                          <input type="email" name="veri_email" class="form-control" value="<?php echo $college->veri_email;?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email Panlok</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $college->coll_email;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">NIP Verifikator</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" maxlength="30" name="veri_nip"  value="<?php echo $college->veri_nip;?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Alamat PTAIN</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $college->coll_address;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Jabatan Verifikator</label>
                        <div class="col-sm-9">
                          <select name="pos_id" id="pos_id" class="form-control">
                            <?php
                            foreach ($position as $value) { ?>
                            <option value="<?php echo $value->pos_id;?>" <?php if($college->pos_id == $value->pos_id){ echo 'selected'; }?> ><?php echo $value->pos_name;?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telepon</label>
                        <div class="col-sm-9">
                        <label class="col-form-label"> <strong><?php echo $college->coll_phone;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telepon Verifikator</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" maxlength="11" name="veri_hp" value="<?php echo $college->veri_hp;?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Website</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $college->coll_site;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <div class="col-sm-2">
                        <button class="btn btn-primary">Simpan</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Kode Wilayah PTAIN</label>
                        <div class="col-sm-9">
                          <label class="col-form-label"> <strong><?php echo $college->coll_region;?></strong></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
		      
        </div>
