
<br>
<br>
<br>
<table class="table table-striped" id="tableseleksi">
  <thead>
      <tr>
        <th>No</th>
        <th style="width: 1px;">
          <?php
          if($permajor->mjr_quota_change - $countMajor->hasil == 0){
            $disable = 'onclick="return false;"';   
          }
          ?>
          <div class="page__toggle">
            <label class="toggle" >
              <input class="toggle__input" id="cek_all" type="checkbox" <?php echo $disable;?> onchange="javascript:cekAll('<?php echo $mjr_code?>');" >
              <span class="toggle__label">
                <span class="toggle__text"></span>
              </span>
            </label>
          </div>
        </th>
        <!-- <th>No</th> -->
        <th style="width: 10px;">Nama Lengkap</th>
        <th>Nomor<br/>Peserta</th>
        <th>Personality</th>
        <th>Nilai</th>
        <?php 
        foreach ($matpel as $value) { ?>
        <th><?php echo $value->mpl_name;?></th>                            
        <?php
        }
        ?>
        <th>Cadangan</th>
        
      </tr>
  </thead>
  <tbody>
    <?php 
    $i = 1;
    $id_noncheck = '';    
    $m = 0;
    foreach ($student as $value) {
      $warna = '';
      $disable = '';
      if($value->std_lulus == '' || $value->std_lulus == null || $value->std_lulus == '0'){
        $checked = '';
      }else {
        $checked = 'checked';
        $cekk = 0;
        // perulangan untuk mengecek prodi tersebut ada atau tidak diuniv itu
        foreach ($majoring as $valued) {
          if($valued->mjr_code == $value->std_lulus){
            $cekk = 1;
            break;
          }
        }
        // kondisi jika ada siswa yang masuk dengan auto tidak bisa diubah
        if(substr($value->std_pilihan,0,1) == 1){
          $warna = 'style="--toggleColor:#000"';
          $disable = 'onclick="return false;"';
        }
        // kondisi jika 0 maka prodi yang diterima tidak ada diuniv tersebut
        // jika 1 maka prodi tersebut ada di univ yang login
        if ($cekk == 0) {
          $warna = 'style="--toggleColor:#cfcfcf"';
          $disable = 'onclick="return false;"';
        }
        if($mjr_code != $value->std_lulus){
          $warna = 'style="--toggleColor:#cfcfcf"';
          $disable = 'onclick="return false;"';
        }
        
      }
      
      if($mjr_code != $value->std_cadangan){
        $warna_cadangan = 'style="--toggleColor:#cfcfcf"';
        $disable_cadangan = 'onclick="return false;"';
      }
      else{
        $warna_cadangan = '';
        $disable_cadangan = '';
      }

      if($value->std_cadangan == ''){
        $checked_cadangan = '';
        $warna_cadangan = '';
        $disable_cadangan = '';
      }
      else{
        $checked_cadangan = 'checked';
      }
      if($college->coll_state == '-1' || $college->coll_state == '0'){
        $disable = 'onclick="return false;"';
      }
      ?>
      <tr>
        <td><?php echo $i++;?> </td>
        <td>
          <div class="page__toggle">
            <label class="toggle" <?php echo $warna;?> >
              <input class="toggle__input" id="<?php echo $value->std_id?>" type="checkbox" <?php echo $disable;?>  <?php echo $checked; ?> onchange="javascript:ceksiswa('<?php echo $value->std_id?>','<?php echo $mjr_code?>');" >
              <span class="toggle__label">
                <span class="toggle__text"></span>
              </span>
            </label>
          </div>
        </td>
        
        <td>
          <?php echo $value->std_name;?> <sup>(p<?php echo $value->cs_id;?>)</sup> (<?php 
            if($value->std_gender == '1'){
              echo "P";
            }else {
              echo "L";
            }
            ?>) 
        </td>
        <td> <a href="javascript:detailsiswa('<?php echo $value->std_id?>');" >
            <span data-toggle="modal" data-target="#viewSiswa"><?php echo $value->std_code?></span>  
          </a> </td>

        <td>
          <?php
          $sumarray = 0;
          $sumnilai = 0;
          $json =  json_decode($value->std_personality_detil);
          // print_r ($value->std_personality_detil);
          foreach ($json as $valuess) {
            if($valuess->item != ''){
              $sumnilai = $sumnilai+($valuess->bobot * $valuess->nilai);
              $sumarray++;
            }
          } 
          // echo $sumarray.",".$sumnilai;
          $resultPersonality = $sumnilai/$sumarray;
          if($resultPersonality == ''){
            $resultPersonality = 0;
            $colorPerson = 'danger';
          }else {
            if ($resultPersonality < 30) {
              $colorPerson = 'danger';
            }else if ($resultPersonality < 39) {
              $colorPerson = 'warning';
            }
            else {
              $colorPerson = 'info';
            }
          }
          echo '<i class="mdi mdi-poll-box text-'.$colorPerson.' icon-lg" style="font-size: 1.3rem;"></i> ';
          echo number_format($resultPersonality,2);
          ?>
        </td>
        <td>
        
        <?php 
        $nilai = 0;
        $std_id = $value->std_id;
        if ($nilaiMatpel[$std_id] != 0 && $nilaiMatpel[$std_id] != '' && $total != '0') {
          $nilai = $total/count($nilaiMatpel[$std_id]) ;
        }
        // $nilai = rand(50,95);
        $nilai = $valueStudent[$std_id]->sm_value;
        // contoh data untuk percobaan
        $cekbox = 0;
        if($nilai >= $range->mt_up){
          $warna = 'success'; 
          $cekbox = 1;
        }else if($nilai < $range->mt_up && $nilai >= $range->mt_mid){
          $warna = 'primary'; //atau info
          $cekbox = 1;
        }else if($nilai < $range->mt_mid && $nilai >= $range->mt_down){
          $warna = 'warning'; 
        }else if($nilai < $range->mt_down){
          $warna = 'danger'; 
        }
        echo '<i class="mdi mdi-cube text-'.$warna.' icon-lg" style="font-size: 1.3rem;"></i> ';
        echo $nilai ;
        ?></td>

        <?php 
        $total = 0;
        
        for ($w=0; $w < count($matpel); $w++) { ?> 
        <td><?php echo number_format($nilaiMatpel[$std_id][$w]->sml_value,2); ?></td>
        <?php
          $total = $total+$nilaiMatpel[$w]->sml_value;
        }
        ?>
        <td>
          <div class="page__toggle">
            <label class="toggle" <?php echo $warna_cadangan;?> >
              <input class="toggle__input" id="cadangan_<?php echo $value->std_id?>" type="checkbox"  <?php echo $disable_cadangan; ?>  <?php echo $checked_cadangan; ?> onchange="javascript:cekcadangan('<?php echo $value->std_id?>','<?php echo $mjr_code?>');" >
              <span class="toggle__label">
                <span class="toggle__text"></span>
              </span>
            </label>
          </div>
        </td>

      </tr>
      
    <?php
      if($cekbox == 1 && $checked == ''){
        $id_noncheck = $value->std_code."|".$id_noncheck;
        $datacek[$m]['std_id'] = $value->std_id;
        $datacek[$m]['nilai'] = $nilai;
        $m++;
      }
    }
    
    function cmp($a, $b) {
      if ($a['nilai'] == $b['nilai']) {
          return 0;
      }
      return ($a['nilai'] > $b['nilai']) ? -1 : 1;
    }
    uasort($datacek, 'cmp');

    $m = 0;
    foreach ($datacek as $value) {
      if ($m == ($permajor->mjr_quota_change - $countMajor->hasil)) {
        break;
      }
      $kop[$m] = $value['std_id'];
      // $kop[$m]['std_code'] = $value['std_code'];
      // $kop[$m]['nilai'] = $value['nilai'];
      $m++;
    }
    $codeStudent = json_encode($kop);
    ?>

  </tbody>
</table>
<textarea style="display:none;" name="id_student" id="id_student" cols="30" rows="10"><?php print_r ($codeStudent);?></textarea>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/tbl_seleksi.css">

<script>
$(document).ready(function() {
    // $('#tableseleksi').DataTable({
    //   "order": [[ 7, "desc" ]],
    //   "aLengthMenu": [50],
    //   "scrollX": true
    // });
    var t = $('#tableseleksi').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 5, 'desc' ]],
        "scrollX": true
    } );
 
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
} );
</script>
