<br>
<br>
<br>
<br>
<br>
<table class="table table-striped">
  <thead>
      <tr>
        <th>No</th>
        <th>Nama Lengkap</th>
        <th>Nomor<br/>Peserta</th>
        <?php 
        foreach ($matpel as $value) { ?>
        <th><?php echo $value->mpl_name;?></th>                            
        <?php
        }
        ?>
        <th>Nilai</th>
      </tr> 
  </thead>
  <tbody>
    <?php 
    $i = 1;
    foreach ($student as $value) { 
      ?>
      <tr>
        <td><?php echo $i++;?> </td>
        <td><?php echo $value->std_name;?> <sup>(p<?php echo $value->cs_id;?>)</sup> </td>
        <td> <?php echo $value->std_code?></td>
        <?php 
        $total = 0;
        $std_id = $value->std_id;
        for ($w=0; $w < count($matpel); $w++) { ?> 
        <td><?php echo $nilaiMatpel[$std_id][$w]->sml_value?></td>
        <?php
          $total = $total+$nilaiMatpel[$w]->sml_value;
        }
        ?>
        <td>
        
        <?php 
        $nilai = 0;
        if ($nilaiMatpel[$std_id] != 0 && $nilaiMatpel[$std_id] != '' && $total != '0') {
          $nilai = $total/count($nilaiMatpel[$std_id]) ;
        }
        // $nilai = rand(50,95);
        $nilai = $valueStudent[$std_id]->sm_value;
        
        echo $nilai ;
        ?></td>
      </tr>
      
    <?php
    }
    ?>

  </tbody>
</table>
              
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/tbl_seleksi.css">

<script>
$(document).ready(function() {
    $('#tableseleksi').DataTable({
      "order": [[ 7, "desc" ]],
      "aLengthMenu": [100],
      orderDataType: 'dom-checkbox'
    });
} );
</script>
