
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card ">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-5"><?php echo $this->session->userdata('name_college');?></h2>
                  <table class="table table-striped">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Prodi</th>
                          <th>Nama Prodi</th>
                          <th> <center>Kuota Awal</center> </th>
                          <th> <center>Kuota Perubahan</center> </th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i = 1;
                      foreach ($prodiPTN as $value) { ?>
                        <tr>
                          <td><?php echo $i++?></td>
                          <td><?php echo $value->mjr_code?></td>
                          <td><?php echo $value->mjr_name?></td>
                          <td> <center><?php echo $value->mjr_quota;?></center> </td>
                          <td> <center>
                          <input type="text" id="<?php echo $value->mjr_code?>" onchange="cekkuota('<?php echo $value->mjr_code?>');" style="width:50px;" class="form-control" value="<?php echo $value->mjr_quota_change?>" >
                          </center> </td>
                        </tr>
                        
                      <?php
                      }
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

<script>
function cekkuota(noid)
{
  let value = $('#'+noid).val();
  $.ajax({
        url: base_url+"panlok/cekkuotaubah",
        type: "post",
        data:  'mjr_id='+noid+'&value='+value,
        success: function (response) {
          let data = JSON.parse(response)
          if(data.cek == 0){
            $('#'+noid).val(data.kuota)
          }
          alert (data.pesan)
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
    });
}
</script>