<?php
class Panlok_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	function getCollege($id_college = 0)
	{
		$this->db->select('a.*,b.*,c.*');
		$this->db->from("college a");
		$this->db->join('verifikator b', 'a.coll_id = b.coll_id','left');
		$this->db->join('position c', 'c.pos_id = b.pos_id','left');
		$this->db->where('a.coll_id  = ', $id_college);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getLogin($log_user_id = 0, $old_password = 0)
	{
		$this->db->select('*');
		$this->db->from("login_at");
		$this->db->where('log_user_id  = ', $log_user_id);
		$this->db->where('password  = ', $old_password);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getKuota($id_college = 0)
	{
		$this->db->select('SUM(mjr_quota) AS quota,SUM(mjr_quota_change) AS quota_change',false);
		$this->db->from("majoring ");
		$this->db->where('coll_id  = ', $id_college);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getCountMajor($mjr_code = 0)
	{
		$this->db->select('COUNT(std_lulus) as hasil',false);
		$this->db->from("student ");
		$this->db->where('std_lulus = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getSumChoice2($coll_id = 0)
	{
		$this->db->select('cs_id,COUNT(b.mjr_code) as hasil',false);
		$this->db->from("student_choice a ");
		$this->db->join('majoring b', 'a.mjr_code = b.mjr_code');
		$this->db->where('b.coll_id  = ', $coll_id);
		$this->db->group_by('cs_id'); 
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getSumStud($id_college = 0)
	{
		$this->db->select('COUNT(DISTINCT(a.std_id)) as hasil',false);
		$this->db->from("student_choice a ");
		$this->db->join('majoring b', 'a.mjr_code = b.mjr_code');
		$this->db->where('coll_id  = ', $id_college);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getSumChoice($mjr_code = 0)
	{
		$this->db->select('cs_id,COUNT(b.mjr_code) as hasil',false);
		$this->db->from("student_choice a ");
		$this->db->join('majoring b', 'a.mjr_code = b.mjr_code');
		$this->db->where('b.mjr_code  = ', $mjr_code);
		$this->db->group_by('cs_id'); 
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getSumGraduate($mjr_code = 0)
	{
		$this->db->select('SUBSTR(std_pilihan,3,1) AS std_pilihans, COUNT(a.std_lulus) AS hasil',false);
		$this->db->from("student a ");
		$this->db->join('majoring b', 'a.std_lulus = b.mjr_code ');
		$this->db->where('b.mjr_code  = ', $mjr_code);
		$this->db->group_by('std_pilihans',false); 
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getMaxStud($id_college = 0)
	{
		$this->db->select('c.std_code, c.std_name, b.mjr_name, sm_value AS jumlah ',false);
		$this->db->from("student_choice a ");
		$this->db->join('majoring b', 'a.mjr_code = b.mjr_code');
		$this->db->join('student c', 'c.std_id = a.std_id');
		$this->db->join('student_major d', 'd.std_id = c.std_id');
		$this->db->where('b.coll_id  = ', $id_college);
		$this->db->group_by('c.std_name'); 
		$this->db->order_by("jumlah", "desc");
		$this->db->limit(10);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getPosition()
	{
		$this->db->select('*');
		$this->db->from("position a");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getMajoring($id_college = 0)
	{
		$this->db->select('*');
		$this->db->from("majoring");
		$this->db->where('coll_id  = ', $id_college);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getPerMajoring($mjr_code = '0')
	{	
		$this->db->select('a.*,b.coll_name');
		$this->db->from("majoring a");
		$this->db->join('college b', 'a.coll_id = b.coll_id',"left");
		$this->db->where('mjr_code  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPerMajorType($mt_id = 0)
	{
		$this->db->select('*');
		$this->db->from("major_type");
		$this->db->where('mt_id = ', $mt_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	function getGraduate($id_college = 0)
	{
		$this->db->select('COUNT(a.std_id) as hasil',false);
		$this->db->from("student a ");
		$this->db->join('majoring b', 'a.std_lulus = b.mjr_code');
		$this->db->where('b.coll_id  = ', $id_college);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	/**
	 * 
	 */
	function getRekapProv($nameProv = '',$matpel)
	{
		foreach ($matpel as $values) {
			$this->db->select('mpl_id, MAX(sml_value) as sml_value');
			$this->db->from("student a");
			$this->db->join('student_matpel b', 'a.std_id = b.std_id');
			$this->db->where('std_province  = ', $nameProv);
			$this->db->where('mpl_id  = ', $values->mpl_id);
			$query = $this->db->get();
			$simpan = $query->result();
			$data[] = $simpan[0];
		}
		return $data;
	}

	/**
	 * fungsi yang digunakan untuk mendapatkan nilai tertinggi pada masing2 bidang
	 */
	function getNilaiTinggi($mpl_id = 0)
	{
		$this->db->select('b.std_code,std_name,std_province,a.sml_value');
		$this->db->from("student_matpel a");
		$this->db->join('student b', 'a.std_id = b.std_id');
		$this->db->where('mpl_id  = ', $mpl_id);
		$this->db->order_by("sml_value", "desc");
		$this->db->limit(5);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}


	function getStudent($mjr_code = 0, $cs_id = 0 )
	{
		$this->db->select('a.std_id,std_name,b.std_code,cs_id,a.mjr_code,mt_id,std_lulus,std_pilihan, std_personality_detil,std_gender, std_cadangan');
		$this->db->from("student_choice a");
		$this->db->join('student b', 'a.std_id = b.std_id');
		$this->db->join('majoring c', 'c.mjr_code = a.mjr_code');
		$this->db->where('a.mjr_code  = ', $mjr_code);
		if($cs_id != 0){
			$this->db->where('a.cs_id = ', $cs_id);
		}
		$this->db->order_by("std_name", "desc");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	
	function cekkuotaMajor($mjr_code = 0)
	{
		$this->db->select('COUNT(a.std_lulus) AS lulus,b.mjr_quota_change',false);
		$this->db->from("student a");
		$this->db->join('majoring b', 'b.mjr_code = a.std_lulus');
		$this->db->where('mjr_code  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPengesahan($coll_id = 0)
	{
		$this->db->select('a.mjr_id,a.mjr_code,a.mjr_name, a.mjr_quota,a.mjr_quota_change, COUNT(b.std_id) AS hasil');
		$this->db->from("majoring a");
		$this->db->join('student b', 'a.mjr_code = b.std_lulus');
		$this->db->where('a.coll_id  = ', $coll_id);
		$this->db->group_by("a.mjr_id");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getPerStudent($std_id = 0 )
	{
		$this->db->select('*');
		$this->db->from("student");
		$this->db->where('std_id  = ', $std_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getValueStudent($std_id = 0 , $mt_id = 0)
	{
		$this->db->select('*');
		$this->db->from("student_major");
		$this->db->where('std_id  = ', $std_id);
		$this->db->where('mt_id  = ', $mt_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPerCodeStudent($std_code = 0 )
	{
		$this->db->select('*');
		$this->db->from("student");
		$this->db->where("std_code  = '$std_code'");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPerCodeCollStudent($std_code = 0, $coll_id = 0 )
	{
		$this->db->select('a.std_id, a.std_name,b.mjr_code, c.coll_id');
		$this->db->from("student a");
		$this->db->join('student_choice b', 'a.std_id = b.std_id');
		$this->db->join('majoring c', 'b.mjr_code = c.mjr_code');
		$this->db->where("std_code  = '$std_code'");
		$this->db->where("coll_id  = '$coll_id'");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPerStudentMatpel($std_id = 0 )
	{
		$this->db->select('a.*,b.mpl_name');
		$this->db->from("student_matpel a");
		$this->db->join('matpel b', 'a.mpl_id = b.mpl_id');
		$this->db->where('std_id  = ', $std_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function cekpilihan($std_id = 0,$mjr_code = 0 )
	{
		$this->db->select('cs_id');
		$this->db->from("student_choice");
		$this->db->where('std_id  = ', $std_id);
		$this->db->where('mjr_code  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0]->cs_id;
	}

	function getPerStudentChoice($std_id = 0 )
	{
		$this->db->select('a.*,b.cs_name,c.mjr_name,d.coll_name,d.coll_id');
		$this->db->from("student_choice a");
		$this->db->join('choices b', 'a.cs_id = b.cs_id');
		$this->db->join('majoring c', 'a.mjr_code = c.mjr_code');
		$this->db->join('college d', 'd.coll_id = c.coll_id');
		$this->db->where('std_id  = ', $std_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getMatpelStudent($std_id = 0)
	{
		$this->db->select('*');
		$this->db->from("student_matpel");
		$this->db->where('std_id  = ', $std_id);
		$this->db->order_by("mpl_id", "ASC");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getMajorGraduate($mjr_code = 0 )
	{
		$this->db->select('a.*,b.mt_id');
		$this->db->from("student a");
		$this->db->join('majoring b', 'b.mjr_code = a.std_lulus');
		$this->db->where("a.std_lulus  = '$mjr_code'");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	
	function getcollegeStud($std_id = 0)
	{
		$this->db->select('std_id,std_code,std_name, mjr_name, coll_name',false);
		$this->db->from("student a ");
		$this->db->join('majoring b', 'a.std_lulus = b.mjr_code ', 'left');
		$this->db->join('college c', 'c.coll_id = b.coll_id', 'left');
		$this->db->where('a.std_id  = ', $std_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

}
?>
