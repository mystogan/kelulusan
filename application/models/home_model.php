<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	/**
	 * function untuk login panlok
	 */
	function login($username,$password)
	{
		$this->db->select('*');
		$this->db->from("login_at");
		$this->db->join('college', 'coll_id = log_user_id');
		$this->db->where('log_user_id =', $username);
		$this->db->where('password =', $password);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	/**
	 * untuk mengambil data dari tabel college
	 */
	function getCollege()
	{
		$this->db->select('*');
		$this->db->from("college");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}


	function getstudent()
	{
		$this->db->select('a.owner_id,a.id');
		$this->db->from("profile a");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getchoice2($id)
	{
		$this->db->select('*');
		$this->db->from("choice");
		$this->db->where('owner_id =', $id);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	/**
	 * untuk mengambil data login admin 
	 */
	function getLoginAdmin()
	{
		$this->db->select('*');
		$this->db->from("login_at");
		$this->db->where('username =', 'admin');
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	/**
	 * function after login adalah untuk memasukkan last login dan history
	 */
	function afterLogin($username = 0, $id_login = 0)
	{
		
		$data['log_id'] = $id_login;
		$data['his_login'] = date("Y-m-d h:i:s");
		$data['his_created'] = date("Y-m-d h:i:s");
		$this->db->insert('login_history', $data);
		$data = null;
		$data['last_login'] = date("Y-m-d h:i:s");
		$this->db->where('log_id', $username);
		$this->db->update('login_at', $data);
		
	}

	/**
	 * function yang digunakan untuk mengisi histori setelah logout
	 */
	function beforeLogout($username = 0, $id_login = 0)
	{
		$this->db->select('*');
		$this->db->from("login_history");
		$this->db->where('log_id =', $id_login);
		$this->db->order_by('his_login', 'DESC');
		$query = $this->db->get();
		$datas = $query->result();
		
		$his_login = $datas[0]->his_login;
		$data['his_logout'] = date("Y-m-d h:i:s");

		$awal  = new DateTime($his_login);
		$akhir = new DateTime($data['his_logout']); 
		$diff  = $awal->diff($akhir);
		
		$data['his_long'] = $diff->h;

		$this->db->where('his_id', $datas[0]->his_id);
		$this->db->update('login_history', $data);	
		
	}


}
?>
