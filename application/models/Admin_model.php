<?php
class Admin_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	/**
	 * function untuk login panlok
	 */
	function getSumStud()
	{
		$this->db->select('count(std_id) as hasil',false);
		$this->db->from("student");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	function getSumSchool()
	{
		$this->db->select('count(sch_id) as hasil',false);
		$this->db->from("school");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	function getSumProv()
	{
		$this->db->select('COUNT(DISTINCT(std_province)) as hasil',false);
		$this->db->from("student");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	function getSumGraduate()
	{
		$this->db->select('count(std_id) as hasil',false);
		$this->db->from("student");
		$this->db->where('std_lulus  != 0');
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}

	function getmatpel()
	{
		$this->db->select('*');
		$this->db->from("matpel");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	
	function getRekapProv($name_prov = 'o')
	{
		$this->db->select('DISTINCT(b.mpl_id), MAX(sml_value) AS sml_value',false);
		$this->db->from("student a");
		$this->db->join('student_matpel b', 'a.std_id = b.std_id');
		$this->db->where('a.std_province  = ', $name_prov);
		$this->db->group_by('b.mpl_id');
		$this->db->order_by('b.mpl_id', 'ASC');
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getPeminat($coll_id = 0)
	{
		$this->db->select('DISTINCT(a.cs_id), COUNT(a.mjr_code) as jumlah, cs_name',false);
		$this->db->from("student_choice a");
		$this->db->join('majoring b', ' a.mjr_code = b.mjr_code');
		$this->db->join('choices c', 'a.cs_id = c.cs_id');
		$this->db->where('b.coll_id  = ', $coll_id);
		$this->db->group_by('a.cs_id');
		$this->db->order_by('a.cs_id', 'ASC');
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getVerifikator()
	{
		$this->db->select('a.coll_name, b.veri_name, b.veri_nip, c.pos_name, b.veri_hp, b.veri_email');
		$this->db->from("college a");
		$this->db->join('verifikator b', 'a.coll_id = b.coll_id','left');
		$this->db->join('position c', 'b.pos_id = c.pos_id','left');
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getTerima($coll_id = 0)
	{
		$this->db->select('DISTINCT(SUBSTRING(a.std_pilihan,3)) AS std_pilihans, COUNT(std_lulus) as jumlah',false);
		$this->db->from("student a");
		$this->db->join('majoring b', ' a.std_lulus = b.mjr_code');
		$this->db->where('b.coll_id  = ', $coll_id);
		$this->db->group_by('std_pilihans'); 
		$this->db->order_by('std_pilihans', 'ASC');
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	/**
	 * query untuk distinct yang digunakan pada menu admin untuk menentukan rekap perprovinsi
	 */
	function getDistProv()
	{
		$this->db->select('DISTINCT(std_province) as prov');
		$this->db->from("student");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getRekapLulus()
	{
		$this->db->select('DISTINCT(a.coll_name),a.coll_id, COUNT(b.coll_id) AS prodi, 
							SUM(mjr_quota) AS kuota, COUNT(c.std_lulus) AS diterima',false);
		$this->db->from("college a");
		$this->db->join('majoring b', 'a.coll_id = b.coll_id');
		$this->db->join('student c', 'b.mjr_code = c.std_lulus','left');
		$this->db->group_by('b.coll_id'); 
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getSumColl()
	{
		$this->db->select('DISTINCT(a.coll_name),a.coll_id, COUNT(b.coll_id) AS prodi,coll_state, coll_confirm,
							SUM(mjr_quota) AS kuota',false);
		$this->db->from("college a");
		$this->db->join('majoring b', 'a.coll_id = b.coll_id');
		$this->db->group_by('b.coll_id'); 
		$this->db->order_by('coll_name', 'ASC');
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	/**
	 * fungsi yang digunakan untuk mendapatkan nilai tertinggi pada masing2 bidang
	 */
	function getNilaiTinggi($mpl_id = 0)
	{
		$this->db->select('b.std_code,std_name,std_province,a.sml_value');
		$this->db->from("student_matpel a");
		$this->db->join('student b', 'a.std_id = b.std_id');
		$this->db->where('mpl_id  = ', $mpl_id);
		$this->db->order_by("sml_value", "desc");
		$this->db->order_by("std_name", "asc");
		$this->db->limit(5);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getAutoChoice($pilihan = 0)
	{
		$this->db->select('b.std_id,std_name, sc_id, mjr_name, c.mt_id, d.mt_type,d.mt_up,c.mjr_code');
		$this->db->from("student_choice a");
		$this->db->join('student b', 'a.std_id = b.std_id');
		$this->db->join('majoring c', 'a.mjr_code = c.mjr_code');
		$this->db->join('major_type d', 'd.mt_id = c.mt_id');
		$this->db->where('a.cs_id  = ', $pilihan);
		$this->db->group_by("a.std_id");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getStudentChoice($pilihan = 0,$mjr_code = 0)
	{
		$this->db->select('b.std_id,std_name, sc_id, c.mt_id, d.mt_type,d.mt_up,c.mjr_code, e.sm_value, b.std_lulus');
		$this->db->from("student_choice a");
		$this->db->join('student b', 'a.std_id = b.std_id');
		$this->db->join('majoring c', 'a.mjr_code = c.mjr_code');
		$this->db->join('major_type d', 'd.mt_id = c.mt_id');
		$this->db->join('student_major e', 'e.std_id = b.std_id AND e.mt_id = c.mt_id');
		$this->db->where('a.cs_id  = ', $pilihan);
		$this->db->where('sm_value != 0');
		$this->db->where('sm_value >= mt_up');
		$this->db->where('std_lulus IS NULL');
		$this->db->where('c.mjr_code = ', $mjr_code);
		$this->db->group_by("a.std_id");
		$this->db->order_by("mjr_code", "asc");
		$this->db->order_by("sm_value", "desc");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}

	function getValueMajor($std_id = 0,$mt_id = 0,$mt_up = 0 )
	{
		$this->db->select('*');
		$this->db->from("student_major");
		$this->db->where('mt_id  = ', $mt_id);
		$this->db->where('std_id  = ', $std_id);
		$this->db->where("sm_value  >= $mt_up ");
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getChoice()
	{
		$this->db->select('*');
		$this->db->from("choices");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getIdStudent($std_code = 0)
	{
		$this->db->select('std_id');
		$this->db->from("student");
		$this->db->where('std_code  = ', $std_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getPerMajoring($mjr_code = 0)
	{
		$this->db->select('*');
		$this->db->from("majoring");
		$this->db->where('mjr_code  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getSumMajoring2($mjr_code = 0)
	{
		$this->db->select('COUNT(a.std_lulus) AS hasil ');
		$this->db->from("student a");
		$this->db->where('std_lulus  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getSetting($setting_name = '0')
	{
		$this->db->select('settings');
		$this->db->from("setting");
		$this->db->where('setting_name  = ', $setting_name);
		$query = $this->db->get();
		$data = $query->result();
		$datas = json_decode($data[0]->settings);
		return $datas;
	}
	function getSumMajoring($mjr_code = 0)
	{
		$this->db->select('COUNT(a.std_lulus) AS hasil ');
		$this->db->from("student a");
		$this->db->join('majoring b', 'a.std_lulus = b.mjr_code');
		$this->db->where('mjr_code  = ', $mjr_code);
		$query = $this->db->get();
		$data = $query->result();
		return $data[0];
	}
	function getStdPrint($coll_id = 0)
	{
		$this->db->select('std_code,std_name,std_gender, mjr_name');
		$this->db->from("student a");
		$this->db->join('majoring b', 'a.std_lulus = b.mjr_code');
		$this->db->where('b.coll_id  = ', $coll_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function updateMhsChoice($std_id = 0, $data)
	{
		$this->db->where('std_id', $std_id);
		$this->db->update('student', $data);
	}

	function updateStateCollege($coll_id = 0, $data)
	{
		if($coll_id != 0){
			$this->db->where('coll_id', $coll_id);
		}
		$this->db->update('college', $data);
	}
	function importMhs($data)
	{
		if($this->db->insert_batch('student',$data)){
			return 1;
		}else {
			$this->db->where('std_id IS NOT NULL');
			$this->db->delete('student');
			return 0;
		}
	}

	function importMatpel($data)
	{
		if($this->db->insert_batch('student_matpel',$data)){
			return 1;
		}else {
			$this->db->where('std_id IS NOT NULL');
			$this->db->delete('student_matpel');
			return 0;
		}
	}
	function importmajoring($data)
	{
		// $this->db->set($data);
		if($this->db->insert_batch('student_major',$data)){
		// if($this->db->insert('student_major')){
			return 1;
		}else {
			$this->db->where('std_id IS NOT NULL');
			$this->db->delete('student_major');
			return 0;
		}
	}
	function importchoice($data)
	{
		// $this->db->set($data);		
		if($this->db->insert_batch('student_choice', $data)){
		// if($this->db->insert('student_choice')){
			return 1;
		}else {
			$this->db->where('std_id IS NOT NULL');
			$this->db->delete('student_choice');
			return 0;
		}
	}










	/**
	 * kumpulan function untuk dummy data
	 */
	function getAllStud()
	{
		$this->db->select('*',false);
		$this->db->from("student");
		$this->db->limit(30000);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getAllMajoring()
	{
		$this->db->select('*',false);
		$this->db->from("kelulusan.majoring");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getlMajoring()
	{
		$this->db->select('*',false);
		$this->db->from("majoring");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getGrade()
	{
		$this->db->select('*');
		$this->db->from("student_grade");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getTypeMajor()
	{
		$this->db->select('*');
		$this->db->from("major_type");
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
	function getStdChoice($std_id)
	{
		$this->db->select('a.*,b.*');
		$this->db->from("student_choice a");
		$this->db->join('majoring b', 'a.mjr_code = b.mjr_code');
		$this->db->where('a.std_id = ', $std_id);
		$query = $this->db->get();
		$data = $query->result();
		return $data;
	}
}
?>
