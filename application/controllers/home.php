<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		SESSION_START();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// if ($this->session->userdata('log_user_id') == '1') {
		// 	redirect(base_url()."admin");
		// }else if($this->session->userdata('log_user_id') > 1){
		// 	redirect(base_url()."panlok");
		// }
	}

	/**
	 * fungsi yang digunakan untuk load pertama pada controller home yaitu untuk load tampilan login
	 */ 
	public function index()
	{
		// $data = '';
		// $this->load->view('admin/header',$data);
		// $this->load->view('admin/index',$data);
		// $this->load->view('admin/footer',$data);
		$this->session->set_userdata('_key',md5(rand()));
		$data['college'] = $this->Home_model->getCollege();
		$data['_err'] = $this->session->userdata('_err');
		$this->load->view('login',$data);
		$this->session->set_userdata('_err','');
	}

	// function digunakan untuk mendapatkan data saja jika sistem akan digunakan atausudah running bisa 
	// dihapus beserta modelnya 
	public function g()
	{
		header("Content-Type: application/xls");    
		header("Content-Disposition: attachment; filename=coba.xls");  
		header("Pragma: no-cache"); 
		header("Expires: 0");
		$student = $this->Home_model->getstudent();
		echo '<table style="width:100%">
		<tr>
			<th>owner</th>
			<th>Code</th>
			<th>Pilihan 1</th> 
			<th>Pilihan 2</th>
			<th>Pilihan 3</th>
		</tr>
		';
		foreach ($student as $value) {
		echo '<tr>
				<td>'.$value->id.'</td>
				<td>'.$value->owner_id.'</td>
			';			
			$coice = $this->Home_model->getchoice2($value->id);
			foreach ($coice as $values) {
				echo '<td>'.$values->majoring.'</td>';
			}
		echo '</tr>';
		
		}
		echo '</table><style>
			table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		  	}</style>';
	}
	
	/**
	 * function yang digunakan untuk mengecek login ketika tampilan login baik admin, maupun panlok
	 */
	function ceklogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$_key = $this->input->post('_key');
		$month = array("Januari", "Februari", "Maret", "April" , "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$days = array("Minggu", "Senin", "Selasa", "Rabu" , "Kamis", "Jum'at", "Sabtu");
		$url = '';
		if($this->session->userdata('_key') == $_key){
			$admin = $this->Home_model->getLoginAdmin();
			if((md5('admin&').$admin->password) == (md5(substr($password,0,6)).md5(substr($password,6,3)))){
				$url = 'admin';
				$this->session->set_userdata('id_login','0');
				$this->session->set_userdata('log_user_id','1');
				$this->session->set_userdata('name_college','Admin');
				$this->session->set_userdata('coll_region','1');
				$this->session->set_userdata('coll_foto','admin.png');
				$this->session->set_userdata('days',$days);
				$this->session->set_userdata('month',$month);
				$this->Home_model->afterLogin($username,'1');
			}else {
				$panlok = $this->Home_model->login($username,md5($password));
				if($panlok[0] != ''){
					$this->session->set_userdata('id_login',$panlok[0]->log_id);
					$this->session->set_userdata('log_user_id',$panlok[0]->log_user_id);
					$this->session->set_userdata('name_college',$panlok[0]->coll_name);
					$this->session->set_userdata('coll_region',$panlok[0]->coll_region);
					$this->session->set_userdata('coll_foto',$panlok[0]->coll_foto);
					$this->session->set_userdata('days',$days);
					$this->session->set_userdata('month',$month);
					$this->Home_model->afterLogin($username,$panlok[0]->log_id);
					$url = 'panlok';
				}else{
					$url = '';
					$this->session->set_userdata('_err','Username atau Password salah');
				}
			}
		}else {
			$url = '';
			$this->session->set_userdata('_err','Ada Kesalahan Silahkan Login Kembali ');
		}
		redirect(base_url().$url);
	}

	/**
	 * function yang digunakan untuk logout semua user
	 */
	public function logout()
	{
		if ($this->session->userdata('id_login') != '') {
			$this->Home_model->beforeLogout($this->session->userdata('log_user_id'),$this->session->userdata('id_login'));			
		}
		$this->session->unset_userdata('id_login');
		$this->session->unset_userdata('log_user_id');
		$this->session->unset_userdata('name_college');
		$this->session->unset_userdata('coll_region');
		$this->session->unset_userdata('coll_foto');
		$this->session->unset_userdata('days');
		$this->session->unset_userdata('month');
		// SESSION_DESTROY();
		redirect(base_url());
	}

	
}
