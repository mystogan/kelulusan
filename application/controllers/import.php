<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {
	function __construct(){ 
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// if ($this->session->userdata('log_user_id') != '1') {
		// 	$this->session->set_userdata('_err','Silahkan Login Kembali');
		// 	redirect(base_url());
		// }
	} 

	public function index()
	{
		// $data['active'] = 'beranda';
		// // start query1 untuk rekap nilai tertinggi 
		// $data['allStud'] = $this->Admin_model->getSumStud();
		// $data['matpel'] = $this->Admin_model->getmatpel();
		// foreach ($data['matpel'] as $value) {
		// 	$mpl_id = $value->mpl_id;
		// 	$data['NilaiTinggi'][] = $this->Admin_model->getNilaiTinggi($mpl_id);
		// }
		// // end query1 
		
		
		// $this->load->view('admin/header',$data);
		// $this->load->view('admin/index',$data);
		// $this->load->view('admin/footer',$data);
		redirect(base_url()."import/mahasiswa");
	}
	
	public function mahasiswa()
	{
		
		$data['_err'] = $this->session->userdata('_err');
		$data['_err2'] = $this->session->userdata('_err2');
		$data['_color'] = $this->session->userdata('_color');
		
		$this->load->view('admin/header',$data);
		$this->load->view('import/mahasiswa',$data);
		$this->load->view('admin/footer',$data);
		$this->session->set_userdata('_err',"");
		$this->session->set_userdata('_err2',"");
		$this->session->set_userdata('_color',"");
		

	}
	public function importMhs()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '-1');
		$time_pre = microtime(true);
		$file = $_FILES['mahasiswa']['tmp_name'];
		$ekstensi  = explode('.', $_FILES['mahasiswa']['name']);
		if (empty($file)) {
			echo 'File tidak boleh kosong!';
		} else {
			if (strtolower(end($ekstensi)) === 'csv' && $_FILES["mahasiswa"]["size"] > 0) {
				$i = 0;
				$k = 0;
				$simpan = '';
				$handle = fopen($file, "r");
				while (($value = fgetcsv($handle, 10000))) {
					if ($i != 0) {
						$data['std_id'] = $value[0];
						$data['sch_npsn'] = $value[1];
						$data['std_name'] = $value[2];
						$data['std_code'] = $value[3];
						$data['std_gender'] = $value[4];
						$data['std_bdate'] = $value[5];
						$data['std_bplace'] = $value[6];
						$data['std_phone'] = $value[7];
						$data['std_address'] = $value[8];
						$data['std_regency'] = $value[9];
						$data['std_province'] = $value[10];
						$data['std_no_tes'] = $value[11];
						$data['std_place_tes'] = $value[12];
						$data['std_year'] = $value[13];
						$data['std_place'] = $value[14];
						$data['std_personality'] = $value[15];
						$data['std_personlity_detil'] = $value[16];
						$data['std_lulus'] = null;
						$data['std_pilihan'] = null;
						$data['std_foto'] = $value[17];
						
						$simpan = array();
						$simpan = $data;
						$k++;
						if($i%200 == 0 ){
							$cek_insert = $this->Admin_model->importMhs($simpan);
							unset($simpan);
							$simpan = array();
						}
					}
					$i++;
				}
				fclose($handle);
				$cek_insert = $this->Admin_model->importMhs($simpan);

			} else {
				echo 'Format file tidak valid!';
			}
		}
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		$this->session->set_userdata('_err2',"Dengan Waktu $exec_time dan jumlah yang masuk $k");	
		if($cek_insert == '1'){
			$this->session->set_userdata('_err',"Import Berhasil");
			$this->session->set_userdata('_color',"blue");
		}
		redirect(base_url()."import/mahasiswa");
	}

	public function matpel()
	{
		
		$data['_err'] = $this->session->userdata('_err');
		$data['_err2'] = $this->session->userdata('_err2');
		$data['_color'] = $this->session->userdata('_color');
		
		$this->load->view('admin/header',$data);
		$this->load->view('import/matpel',$data);
		$this->load->view('admin/footer',$data);
		$this->session->set_userdata('_err',"");
		$this->session->set_userdata('_err2',"");
		$this->session->set_userdata('_color',"");
	}
	public function importmatpel()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '-1');
		$time_pre = microtime(true);
		$file = $_FILES['matpel']['tmp_name'];
		$cek_insert = 0;
		$ekstensi  = explode('.', $_FILES['matpel']['name']);
		if (empty($file)) {
			$this->session->set_userdata('_err',"File tidak boleh kosong!");
		} else {
			if (strtolower(end($ekstensi)) === 'csv' && $_FILES["matpel"]["size"] > 0) {
				$i = 0;
				$k = 0;
				$handle = fopen($file, "r");
				while (($value = fgetcsv($handle, 100000000))) {
					if ($i != 0) {
						$getIdStudent = $value[0];
						$data['std_id'] = $getIdStudent;
						for ($j=1; $j < count($value); $j++) { 
							if($value[$j] == '' || $value[$j] == null){
								break;
							}
							$data['mpl_id'] = $j;
							$data['sml_value'] = $value[$j];	
							$simpan[] = $data;
							$k++;
							$cekpak = $k;
						}
						if($i%200 == 0 ){
							$cek_insert = $this->Admin_model->importMatpel($simpan);
							unset($simpan);
							$simpan = array();
						}
					}
					$i++;
				}
				fclose($handle);
				$cek_insert = $this->Admin_model->importMatpel($simpan);

			} else {
				$this->session->set_userdata('_err',"Format file tidak valid!");
			}
		}
		
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		$this->session->set_userdata('_err2',"Dengan Waktu $exec_time dan jumlah yang masuk $k");	
		if($cek_insert == '1'){
			$this->session->set_userdata('_err',"Import Berhasil");
			$this->session->set_userdata('_color',"blue");
		}
		redirect(base_url()."import/matpel");
	}
	
	public function majoring()
	{
		
		$data['_err'] = $this->session->userdata('_err');
		$data['_err2'] = $this->session->userdata('_err2');
		$data['_color'] = $this->session->userdata('_color');
		
		$this->load->view('admin/header',$data);
		$this->load->view('import/majoring',$data);
		$this->load->view('admin/footer',$data);
		$this->session->set_userdata('_err',"");
		$this->session->set_userdata('_err2',"");
		$this->session->set_userdata('_color',"");
	}

	
	public function importmajoring()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '-1');
		$time_pre = microtime(true);
		$file = $_FILES['majoring']['tmp_name'];
		$ekstensi  = explode('.', $_FILES['majoring']['name']);
		if (empty($file)) {
			$this->session->set_userdata('_err',"File tidak boleh kosong!");
		} else {
			if (strtolower(end($ekstensi)) === 'csv' && $_FILES["majoring"]["size"] > 0) {
				$k = 0;
				$i = 0;
				$handle = fopen($file, "r");
				while (($value = fgetcsv($handle, 10000))) {
					if($value[1] == '' || $value[1] == null){
						break;
					}
					if ($i != 0) {
						// $getIdStudent = $this->Admin_model->getIdStudent($value[0]);
						$data['std_id'] = $value[0];
						for ($j=1; $j < count($value); $j++) { 
							if($value[$j] == '' || $value[$j] == null){
								break;
							}
							$data['mt_id'] = $j;
							$data['sm_value'] = $value[$j];
							$simpan[] = $data;
							$k++;
							$cekpak = $k;
							
						}
						if($i%200 == 0 ){
							$cek_insert = $this->Admin_model->importmajoring($simpan);
							unset($simpan);
							$simpan = array();
						}
					}
					$i++;
				}
				fclose($handle);
				$cek_insert = $this->Admin_model->importmajoring($simpan);
			} else {
				$this->session->set_userdata('_err',"Format file tidak valid!");
			}
		}
		
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		$this->session->set_userdata('_err2',"Dengan Waktu $exec_time dan jumlah yang masuk $k");	
		if($cek_insert == '1'){
			$this->session->set_userdata('_err',"Import Berhasil");
			$this->session->set_userdata('_color',"blue");
		}
		redirect(base_url()."import/majoring");
	}


	public function choice()
	{
		$data['_err'] = $this->session->userdata('_err');
		$data['_err2'] = $this->session->userdata('_err2');
		$data['_color'] = $this->session->userdata('_color');
		
		$this->load->view('admin/header',$data);
		$this->load->view('import/choice',$data);
		$this->load->view('admin/footer',$data);
		$this->session->set_userdata('_err',"");
		$this->session->set_userdata('_err2',"");
		$this->session->set_userdata('_color',"");
	}

	
	public function importchoice()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '-1');
		$time_pre = microtime(true);
		$choice = $this->Admin_model->getChoice();
		$file = $_FILES['choice']['tmp_name'];
		$ekstensi  = explode('.', $_FILES['choice']['name']);
		if (empty($file)) {
			$this->session->set_userdata('_err',"File tidak boleh kosong!");
		} else {
			if (strtolower(end($ekstensi)) === 'csv' && $_FILES["choice"]["size"] > 0) {
				$k = 0;
				$i = 0;
				$handle = fopen($file, "r");
				while (($value = fgetcsv($handle, 10000))) {
					if($value[1] == '' || $value[1] == null){
						break;
					}
					if ($i != 0) {
						// $getIdStudent = $this->Admin_model->getIdStudent($value[0]);
						$data['std_id'] = $value[0];
						for ($j=1; $j <= count($choice); $j++) { 
							if($value[$j] == '' || $value[$j] == null){
								break;
							}
							$data['cs_id'] = $j;
							$data['mjr_code'] = $value[$j];
							$simpan[] = $data;
							// $cek_insert = $this->Admin_model->importchoice($data);
							// if($cek_insert == '0'){
							// 	$this->session->set_userdata('_err',"Ada Kesalahan pada id ke $i");
							// 	$this->session->set_userdata('_color',"red");
							// 	break;
							// }
							$k++;
						}
						if($i%200 == 0 ){
							$cek_insert = $this->Admin_model->importchoice($simpan);
							unset($simpan);
							$simpan = array();
						}
					}
					$i++;
				}
				fclose($handle);
				$cek_insert = $this->Admin_model->importchoice($simpan);

			} else {
				$this->session->set_userdata('_err',"Format file tidak valid!");
			}
		}
		
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		$this->session->set_userdata('_err2',"Dengan Waktu $exec_time dan jumlah yang masuk $k");	
		if($cek_insert == '1'){
			$this->session->set_userdata('_err',"Import Berhasil");
			$this->session->set_userdata('_color',"blue");
		}
		redirect(base_url()."import/choice");
	}

}
