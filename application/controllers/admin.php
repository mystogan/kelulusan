<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once __DIR__ . '/../../vendor/autoload.php';

class Admin extends CI_Controller {
	function __construct(){ 
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		if ($this->session->userdata('log_user_id') != '1') {
			$this->session->set_userdata('_err','Silahkan Login Kembali');
			redirect(base_url());
		}
	} 

	public function index()
	{
			redirect(base_url()."admin/rekapmax");
	}

	
	public function rekapmax()
	{
		// start query1 untuk rekap nilai tertinggi 
		$data['allStud'] = $this->Admin_model->getSumStud();
		$data['allgraduate'] = $this->Admin_model->getSumGraduate();
		$data['allSchool'] = $this->Admin_model->getSumSchool();
		$data['allProv'] = $this->Admin_model->getSumProv();
		$data['matpel'] = $this->Admin_model->getmatpel();
		foreach ($data['matpel'] as $value) {
			$mpl_id = $value->mpl_id;
			$data['NilaiTinggi'][] = $this->Admin_model->getNilaiTinggi($mpl_id);
		}
		// print_r ($data['NilaiTinggi']);
		$data['rekapmax'] = 'active';
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/rekapmax',$data);
		$this->load->view('admin/footer',$data);
	}
	
	public function perprov()
	{
		// start query1 untuk rekap nilai tertinggi 
		$data['allStud'] = $this->Admin_model->getSumStud();
		$data['allgraduate'] = $this->Admin_model->getSumGraduate();
		$data['allSchool'] = $this->Admin_model->getSumSchool();
		$data['allProv'] = $this->Admin_model->getSumProv();
		$data['matpel'] = $this->Admin_model->getmatpel();

		$this->load->view('admin/header',$data);
		$this->load->view('admin/perprov',$data);
		$this->load->view('admin/footer',$data);
	}

	
	public function getprovince()
	{
		$result = $this->Admin_model->getDistProv();
		echo json_encode($result);
	}
	public function getMaxProv()
	{
		$prov = $this->input->post('prov');
		$name_prov = $prov['prov'];
		$result = $this->Admin_model->getRekapProv($name_prov);
		$td = '<tr><td>'.$name_prov.'</td>';
		foreach ($result as $value) {
			$td = $td.'<td>'.$value->sml_value.'</td>';
		}
		$td = $td.'<tr>';
		echo $td;
	}

	public function getRekapLulus()
	{
		$getRekapLulus = $this->Admin_model->getRekapLulus();
		$kopBot = '';
		$total = 0;
		foreach ($getRekapLulus as $value) {
			$kopBot = $kopBot.'
						<tr>
							<td>'.$value->coll_name.'</td>'.
							'<td>'.$value->prodi.'</td>'.
							'<td>'.$value->kuota.'</td>'.
							'<td>'.$value->diterima.'</td>'.
						'<tr>'
						;
			$total = $total+$value->diterima;
		}
		echo $total;
		echo '<thead>
				<tr>
					<th>Nama Instansi</th>
					<th>Prodi</th>
					<th>Kuota</th>
					<th>Diterima</th>
				</tr>
			</thead>
			<tbody>
				'.$kopBot.'
			</tbody>';
	}


	public function rekaplulus()
	{
		$data['active'] = 'rekaplulus';
		$data['allStud'] = $this->Admin_model->getSumStud();
		$data['allgraduate'] = $this->Admin_model->getSumGraduate();
		$data['allSchool'] = $this->Admin_model->getSumSchool();
		$data['allProv'] = $this->Admin_model->getSumProv();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/rekaplulus');
		$this->load->view('admin/footer',$data);
	}
	public function rekapitulasi()
	{
		$data['active'] = 'rekapitulasi';

		$this->load->view('admin/header',$data);
		$this->load->view('admin/rekapitulasi');
		$this->load->view('admin/footer',$data);
	}
	public function batalkan()
	{
		
		$data['_err'] = $this->session->userdata('_err');
		$data['_err2'] = $this->session->userdata('_err2');
		$data['_color'] = $this->session->userdata('_color');

		$data['active'] = 'rekapitulasi';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/batalkan',$data);
		$this->load->view('admin/footer',$data);
		
		$this->session->set_userdata('_err',"");
		$this->session->set_userdata('_err2',"");
		$this->session->set_userdata('_color',"");
	}
	public function siswaBatal($kodeSiswa)
	{
		$this->db->set('std_lulus', 'null', FALSE);
		$this->db->set('std_pilihan', 'null', FALSE);
		$this->db->where('std_code', $kodeSiswa);
		$this->db->update('student');
		$this->session->set_userdata('_err',"Pembatalan Berhasil");
		$this->session->set_userdata('_color',"blue");
		header('Location: '.base_url()."admin/batalkan");
	}
	public function checkstudent($std_code = 0)
	{
		$this->load->model('Panlok_model');
		$std_code = trim($std_code);
		$student = $this->Panlok_model->getPerCodeStudent($std_code);
		$studentColl = $this->Panlok_model->getPerCodeCollStudent($std_code, $this->session->userdata('log_user_id'));
		$data['cek'] = 1;
		$data['std_id'] = $student->std_id;
		echo json_encode($data);
		// echo $mjr_id." dan ".$value;
	}
	public function getrekapitulasi()
	{
		$getSumColl = $this->Admin_model->getSumColl();
		$getChoice = $this->Admin_model->getChoice();
		$table = '<table class="table" >';
		$table = $table.'<tr>';
		$table = $table.'<th rowspan="2" style="width:10px;">No</th>';
		// $table = $table.'<th rowspan="2">Kode<br/>PTKIN</th>';
		$table = $table.'<th rowspan="2">Nama<br/>Instansi</th>';
		$table = $table.'<th rowspan="2">Prodi</th>';
		$table = $table.'<th rowspan="2">Total<br/>Kuota</th>';
		$table = $table.'<th colspan="'.(count($getChoice)+1).'">Peminat</th>';
		$table = $table.'<th colspan="'.(count($getChoice)+1).'">Diterima</th>';
		$table = $table.'</tr>';
		$table = $table.'<tr>';
		foreach ($getChoice as $values) {
			$table = $table.'<th>Pil'.$values->cs_id.'</th>';
		}
		$table = $table.'<th>Total</th>';
		foreach ($getChoice as $values) {
			$table = $table.'<th>Pil'.$values->cs_id.'</th>';
		}
		$table = $table.'<th>Total</th>';
		$table = $table.'</tr>';
		$i =  1;
		foreach ($getSumColl as $value) {
			$table = $table.'<tr>';
			$table = $table.'<td>'.$i++.'</td>';
			// $table = $table.'<td>'.$value->coll_id.'</td>';
			$table = $table.'<td>'.$value->coll_name.'</td>';
			$table = $table.'<td>'.$value->prodi.'</td>';
			$table = $table.'<td>'.$value->kuota.'</td>';
			$getPeminat = $this->Admin_model->getPeminat($value->coll_id);
			$jumlPeminat = 0;
			for ($k=0; $k < count($getChoice); $k++) { 
				$table = $table.'<td>'.$getPeminat[$k]->jumlah.'</td>';		
				$jumlPeminat = $jumlPeminat+$getPeminat[$k]->jumlah;
			}
			// foreach ($getPeminat as $values) {
			// 	$table = $table.'<td>'.$values->jumlah.'</td>';		
			// 	$jumlPeminat = $jumlPeminat+$values->jumlah;
			// }
			$table = $table.'<td>'.$jumlPeminat.'</td>';
			$getTerima = $this->Admin_model->getTerima($value->coll_id);
			$jumTerima = 0;
			for ($k=0; $k < count($getChoice); $k++) { 
				$table = $table.'<td>'.$getTerima[$k]->jumlah.'</td>';
				$jumTerima = $jumTerima+$getTerima[$k]->jumlah;
			}
			// foreach ($getTerima as $values) {
			// 	$table = $table.'<td>'.$values->jumlah.'</td>';
			// 	$jumTerima = $jumTerima+$values->jumlah;
			// }
			$table = $table.'<td>'.$jumTerima.'</td>';
			$table = $table.'</tr>';
		}

		$table = $table.'</table>';
		echo $table;
		
	}
	public function tahapan()
	{
		$data['choice'] = $this->Admin_model->getChoice();
		$data['warna'] = array("primary", "success", "warning", "info", "secondary");
		$this->load->view('admin/header',$data);
		$this->load->view('admin/tahapan');
		$this->load->view('admin/footer',$data);
	}

	public function getCollegeIndividu()
	{
		$sum = $this->Admin_model->getSumColl();
		echo json_encode($sum);
	}
	public function getTabelTahapan()
	{
		$getChoice = $this->Admin_model->getChoice();
		$table = '<table class="table" id="tableTahapan" >';
		$table = $table.'<tr>';
		$table = $table.'<th style="width:10px;">Kode</th>';
		$table = $table.'<th style="width:80px;">Nama<br/>Instansi</th>';
		$table = $table.'<th>Prodi</th>';
		$table = $table.'<th>Total<br/>Kuota</th>';
		foreach ($getChoice as $values) {
			$table = $table.'<th>Pil'.$values->cs_id.'</th>';
		}
		$table = $table.'<th>Total</th>';
		$table = $table.'<th>Status</th>';
		$table = $table.'<th>Intervensi</th>';
		$table = $table.'<th>Download</th>';
		$table = $table.'</tr>';
		$table = $table.'</table>';
		echo $table;
	}

	public function gettahapan()
	{
		$college = $this->input->post('college');
		$no = $this->input->post('no');
		$value = (object) $college;
		$getChoice = $this->Admin_model->getChoice();
		
		if($value->coll_state == '-1'){
			$bg = 'style="background-color:#E3E3E3"';
			$tahap = '';
		}else if($value->coll_state == '-2'){
			$bg = 'style="background-color:#FEC98D"';
			$tahap = '';
		}else if($value->coll_state == '0'){
			$bg = 'style="background-color:#2623D2"';
			$tahap = '';
		}else {
			$bg = 'style="background-color:#CEFECE"';
			$tahap = '(Tahap '.$value->coll_state.')';
		}
		if($value->coll_confirm == 1){
			$confirm = "Sudah disahkan";
		}else {
			$confirm = "Belum";
		}
		$coll_id = $value->coll_id;
		$table = $table.'<tr '.$bg.'>';
		$table = $table.'<td>'.$coll_id.'</td>';
		// $table = $table.'<td>'.$value->coll_id.'</td>';
		$table = $table.'<td>'.$value->coll_name.'</td>';
		$table = $table.'<td>'.$value->prodi.'</td>';
		$table = $table.'<td>'.$value->kuota.'</td>';
		$getTerima = $this->Admin_model->getTerima($coll_id);
		$jumlPeminat = 0;
		$tahapan = '';
		// print_r ($getTerima);
		for ($i=0; $i < count($getChoice); $i++) {
			$kolomtahap = '';
			if($value->coll_state == $getChoice[$i]->cs_id){
				$kolomtahap = 'style="background-color:#F37453"';
			}
			if ($getTerima[$i] == '') {
				$getTerima[$i]->jumlah = 0;
				$table = $table.'<td '.$kolomtahap.' >'.$getTerima[$i]->jumlah.'</td>';		
			}else {
				$table = $table.'<td '.$kolomtahap.' >'.$getTerima[$i]->jumlah.'</td>';		
				$tahapan = $tahapan.'<a class="dropdown-item" href="javascript:tutupkelulusan('.$value->coll_id.','.$getTerima[$i]->std_pilihans.', `Buka Pilihan '.$getTerima[$i]->std_pilihans.'`);">
							<i class="fa fa-reply fa-fw"></i>Buka PIlihan '.$getTerima[$i]->std_pilihans.'</a>';
			}
			$jumlPeminat = $jumlPeminat+$getTerima[$i]->jumlah;

		}
		$tahapan = $tahapan."<a class='dropdown-item' href='javascript:tutupkelulusan(".$value->coll_id.",`-2` , ` Intervensi Langsung ".$value->coll_name."`)'>
						<i class='fa fa-reply fa-fw'></i>Afirmasi</a>";
		$tahapan = $tahapan."<a class='dropdown-item' href='javascript:tutupkelulusan(".$value->coll_id.",`-1` , ` Menutup Kelulusan ".$value->coll_name."`)'>
						<i class='fa fa-reply fa-fw'></i>Tutup Kelulusan</a>";
		$table = $table.'<td>'.$jumlPeminat.'</td>';
		$table = $table.'<td>'.$confirm.'</td>';
		$table = $table.'<td><div class="ticket-actions col-md-2">
		<div class="btn-group dropdown">
			<button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Tahapan
			</button>
			<div class="dropdown-menu">
			'.$tahapan.'
			</div>
		</div>
		</div></td>';
		$table = $table.'<td><a class="btn btn-info" target="_blank" href="'.base_url().'admin/cetak/'.$value->coll_id.'">Cetak</a></td>';
		$table = $table.'</tr>';

		echo $table;
		
	}
	// backup
	public function gettahapan2()
	{
		$getSumColl = $this->Admin_model->getSumColl();
		$getChoice = $this->Admin_model->getChoice();
		$table = '<table class="table" >';
		$table = $table.'<tr>';
		$table = $table.'<th style="width:10px;">No</th>';
		// $table = $table.'<th rowspan="2">Kode<br/>PTKIN</th>';
		$table = $table.'<th style="width:80px;">Nama<br/>Instansi</th>';
		$table = $table.'<th>Prodi</th>';
		$table = $table.'<th>Total<br/>Kuota</th>';
		foreach ($getChoice as $values) {
			$table = $table.'<th>Pil'.$values->cs_id.'</th>';
		}
		$table = $table.'<th>Total</th>';
		$table = $table.'<th>Intervensi</th>';
		$table = $table.'<th>Download</th>';
		$table = $table.'</tr>';
		$i =  1;
		foreach ($getSumColl as $value) {
			if($value->coll_state == '-1'){
				$bg = 'style="background-color:#E3E3E3"';
				$tahap = '';
			}else if($value->coll_state == '-2'){
				$bg = 'style="background-color:#FEC98D"';
				$tahap = '';
			}else {
				$bg = 'style="background-color:#CEFECE"';
				$tahap = '(Tahap '.$value->coll_state.')';
			}
			$table = $table.'<tr '.$bg.'>';
			$table = $table.'<td>'.$i++.'</td>';
			// $table = $table.'<td>'.$value->coll_id.'</td>';
			$table = $table.'<td>'.$value->coll_name.$tahap.'</td>';
			$table = $table.'<td>'.$value->prodi.'</td>';
			$table = $table.'<td>'.$value->kuota.'</td>';
			$getTerima = $this->Admin_model->getTerima($value->coll_id);
			$jumlPeminat = 0;
			$tahapan = '';
			// print_r ($getTerima);
			for ($i=0; $i < count($getChoice); $i++) {
				if ($getTerima[$i] == '') {
					$getTerima[$i]->jumlah = 0;
					$table = $table.'<td>'.$getTerima[$i]->jumlah.'</td>';		
				}else {
					$table = $table.'<td>'.$getTerima[$i]->jumlah.'</td>';		
					$tahapan = $tahapan.'<a class="dropdown-item" href="javascript:tutupkelulusan('.$value->coll_id.','.$getTerima[$i]->std_pilihan.', `Buka Pilihan '.$getTerima[$i]->std_pilihan.'`);">
								<i class="fa fa-reply fa-fw"></i>Buka PIlihan '.$getTerima[$i]->std_pilihan.'</a>';
				}
				$jumlPeminat = $jumlPeminat+$getTerima[$i]->jumlah;

			}
			$tahapan = $tahapan."<a class='dropdown-item' href='javascript:tutupkelulusan(".$value->coll_id.",`-2` , ` Intervensi Langsung ".$value->coll_name."`)'>
							<i class='fa fa-reply fa-fw'></i>Intervensi</a>";
			$tahapan = $tahapan."<a class='dropdown-item' href='javascript:tutupkelulusan(".$value->coll_id.",`-1` , ` Menutup Kelulusan ".$value->coll_name."`)'>
							<i class='fa fa-reply fa-fw'></i>Tutup Kelulusan</a>";
			$table = $table.'<td>'.$jumlPeminat.'</td>';
			$table = $table.'<td><div class="ticket-actions col-md-2">
			<div class="btn-group dropdown">
			  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Tahapan
			  </button>
			  <div class="dropdown-menu">
				'.$tahapan.'
			  </div>
			</div>
		  </div></td>';
			$table = $table.'<td><a class="btn btn-info" href="'.base_url().'admin/cetak/'.$value->coll_id.'">Cetak</a></td>';
			$table = $table.'</tr>';
		}

		$table = $table.'</table>';
		echo $table;
		
	}

	public function verifikator()
	{
		$data['verifikator'] = $this->Admin_model->getVerifikator();
		// print_r ($data['verifikator']);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/verifikator',$data);
		$this->load->view('admin/footer',$data);
	}


	public function autoChoice($pilihan = 0)
	{
		
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '-1');
		$state['coll_state'] = '0';
		$this->Admin_model->updateStateCollege(0,$state);
		$getPerMajoring = $this->Admin_model->getlMajoring();
		print_r ($getPerMajoring);
		foreach ($getPerMajoring as $values) {
			$majoring[$values->mjr_code] = $values->mjr_quota_change;
			$result = $this->Admin_model->getStudentChoice($pilihan,$values->mjr_code);
			foreach ($result as $value) {
				$mjr_code = $values->mjr_code;
				$std_id = $value->std_id;
				$sumMajoring = $this->Admin_model->getSumMajoring2($mjr_code);
				// print_r ($sumMajoring);
				if($value->std_lulus == null){
					$updStd['std_lulus'] = $mjr_code;
					$updStd['std_pilihan'] = '1|'.$pilihan;
					// echo "kuota kode $mjr_code : ".$majoring[$mjr_code]." yang sudah terisi ".$sumMajoring->hasil."<br/>";
					if(($sumMajoring->hasil) >= $majoring[$mjr_code]){
						break;
					}
					$data['verifikator'] = $this->Admin_model->updateMhsChoice($std_id, $updStd);
				}
				
			}
		}
		$o = 1;
		
		// echo json_encode($result);
		redirect(base_url()."admin/tahapan");
	}

	public function autotomatic()
	{
		$student = $this->input->post('student');
		// $result = $this->Admin_model->getAutoChoice($pilihan);
		echo json_encode($data);
	}

	public function autotomatic3($pilihan = 0)
	{

		$i=0;
		// print_r ($getAutoChoice);
		foreach ($getAutoChoice as $value) {
			$getValueMajor = $this->Admin_model->getValueMajor($value->std_id,$value->mt_id,$value->mt_up);
			if($getValueMajor != ''){
				$mjr_code[$i] = $value->mjr_code;
				$data[$i]['mjr_code'] = $value->mjr_code;
				// $data[$i]['mt_up'] = $value->mt_up;
				$data[$i]['std_id'] = $value->std_id;
				$data[$i]['mt_id'] = $value->mt_id;
				$data[$i]['piihan'] = '1';
				$data[$i]['sm_value'] = $getValueMajor->sm_value;
				$i++;
			}
		}

		$mjr_code1 = array_unique($mjr_code);
		$i=0;
		foreach ($mjr_code1 as $value) {
			$mjr_code2[$i] = $value;
			$i++;
		}

		$i=0;
		foreach ($data as $value) {
			$mjr_code = $value['mjr_code'];
			$permajor[$mjr_code][$i]['std_id'] = $value['std_id'];
			$permajor[$mjr_code][$i]['mt_id'] = $value['mt_id'];
			$permajor[$mjr_code][$i]['piihan'] = $value['piihan'];
			$permajor[$mjr_code][$i]['sm_value'] = $value['sm_value'];
			$i++;
		}

		for ($i=0; $i < count($mjr_code2); $i++) {
			$index = $mjr_code2[$i];
			usort($permajor[$index], function($a, $b) {
				if($a['sm_value'] == $b['sm_value']) return 0;
				return $a['sm_value'] < $b['sm_value']?1:-1;
			});
			$o = 1;
			foreach ($permajor[$index] as $value) {
				$getPerMajoring = $this->Admin_model->getPerMajoring($index);
				$this->load->model('Panlok_model');
				$getPerStudent = $this->Panlok_model->getPerStudent($value['std_id']);
				$sumMajoring = $this->Admin_model->getSumMajoring($index);
				
				if($getPerStudent->std_lulus == null){
					$updStd['std_lulus'] = $index;
					$updStd['std_pilihan'] = '1|'.$pilihan;
					$data['verifikator'] = $this->Admin_model->updateMhsChoice($value['std_id'], $updStd);
					if($getPerMajoring->mjr_quota_change == ($o+$sumMajoring->hasil)){
						break;
					}
					$o++;
				}
				
			}
		}
		// print_r ($permajor);
		// redirect(base_url()."admin/tahapan");

	}

	// untuk backup jika running hapus saja
	public function autotomatic2($pilihan = 0)
	{
		$getAutoChoice = $this->Admin_model->getAutoChoice($pilihan);
		$i=0;
		// print_r ($getAutoChoice);
		foreach ($getAutoChoice as $value) {
			$getValueMajor = $this->Admin_model->getValueMajor($value->std_id,$value->mt_id,$value->mt_up);
			if($getValueMajor != ''){
				$mjr_code[$i] = $value->mjr_code;
				$data[$i]['mjr_code'] = $value->mjr_code;
				// $data[$i]['mt_up'] = $value->mt_up;
				$data[$i]['std_id'] = $value->std_id;
				$data[$i]['mt_id'] = $value->mt_id;
				$data[$i]['piihan'] = '1';
				$data[$i]['sm_value'] = $getValueMajor->sm_value;
				$i++;
			}
		}

		$mjr_code1 = array_unique($mjr_code);
		$i=0;
		foreach ($mjr_code1 as $value) {
			$mjr_code2[$i] = $value;
			$i++;
		}

		$i=0;
		foreach ($data as $value) {
			$mjr_code = $value['mjr_code'];
			$permajor[$mjr_code][$i]['std_id'] = $value['std_id'];
			$permajor[$mjr_code][$i]['mt_id'] = $value['mt_id'];
			$permajor[$mjr_code][$i]['piihan'] = $value['piihan'];
			$permajor[$mjr_code][$i]['sm_value'] = $value['sm_value'];
			$i++;
		}

		for ($i=0; $i < count($mjr_code2); $i++) {
			$index = $mjr_code2[$i];
			usort($permajor[$index], function($a, $b) {
				if($a['sm_value'] == $b['sm_value']) return 0;
				return $a['sm_value'] < $b['sm_value']?1:-1;
			});
			$o = 1;
			foreach ($permajor[$index] as $value) {
				$getPerMajoring = $this->Admin_model->getPerMajoring($index);
				$this->load->model('Panlok_model');
				$getPerStudent = $this->Panlok_model->getPerStudent($value['std_id']);
				$sumMajoring = $this->Admin_model->getSumMajoring($index);
				print_r($sumMajoring);
				// if($getPerStudent->std_lulus == null){
				// 	$updStd['std_lulus'] = $index;
				// 	$updStd['std_pilihan'] = '1|'.$pilihan;
				// 	// print_r ($updStd);
				// 	// echo $value['std_id'];
				// 	$data['verifikator'] = $this->Admin_model->updateMhsChoice($value['std_id'], $updStd);
					
				// 	if($getPerMajoring->mjr_quota_change == ($o+$sumMajoring->hasil)){
				// 		break;
				// 	}
				// 	$o++;
				// }
				
			}
		}
		// print_r ($permajor);
		// redirect(base_url()."admin/tahapan");

	}
	public function tutupkelulusan($id,$index)
	{
		$data['coll_state'] = $index;
		$asd = $this->Admin_model->updateStateCollege($id, $data);

	}
	public function cetak($coll_id = 0)
	{
		$data['cetak'] = $this->Admin_model->getSetting('CetakLaporan');
		$data['student'] = $this->Admin_model->getStdPrint($coll_id);
		$this->load->view('admin/cetak', $data);
	}





	/**
	 * untuk mengisi tabel
	 */
	public function isiTablemajoring()
	{
		$allMajor = $this->Admin_model->getAllMajoring();		
		
		$i=1;
		foreach ($allMajor as $value) {
			$data['mjr_id'] = $i++;
			$data['mt_id'] = rand(1,3);
			$data['coll_id'] = $value->co_id;
			$data['mjr_code'] = $value->code;
			$data['mjr_name'] = $value->name;
			$data['mjr_quota'] = $value->quota;
			$data['mjr_quota_change'] = $value->stake;
			$data['mjr_created'] = date('Y-m-d h:i:s');
			// print_r ($data);
			$this->db->insert('majoring', $data);
		}
	}

	public function isiTableStudentChoice()
	{
		$allStud = $this->Admin_model->getAllStud();		
		$choice = $this->Admin_model->getChoice();		
		$getlMajoring = $this->Admin_model->getlMajoring();		
		// print_r ();
		$i=1;
		foreach ($allStud as $value) {
			foreach ($choice as $values) {
				$data['sc_id'] = $i++;
				$data['mjr_code'] = $getlMajoring[rand(1,1113)]->mjr_code;
				$data['cs_id'] = $values->cs_id;
				$data['std_id'] = $value->std_id;
				// print_r ($data);
				$this->db->insert('student_choice', $data);
			}
		}
	}
	public function isiTableStudentmatpel()
	{
		$allStud = $this->Admin_model->getAllStud();		
		$getmatpel = $this->Admin_model->getmatpel();		
		$i=1;
		foreach ($allStud as $value) {
			foreach ($getmatpel as $values) {
				$data['sml_id'] = $i++;
				$data['sml_value'] = rand(500,800);
				$data['mpl_id'] = $values->mpl_id;
				$data['std_id'] = $value->std_id;
				// print_r ($data);
				$this->db->insert('student_matpel', $data);
			}
		}
	}
	public function isiTableLulusStudent()
	{
		$allStud = $this->Admin_model->getAllStud();		
		$i=1;
		foreach ($allStud as $value) {
			$getStdChoice = $this->Admin_model->getStdChoice($value->std_id);		
			$cs_id = '';
			$mjr_id = '';
			foreach ($getStdChoice as $values) {
				$cs_id[] = $values->cs_id;
				$mjr_id[] = $values->mjr_code;
				
			}
			$index = rand(0,2);
			$data['std_pilihan'] = $cs_id[$index];
			$data['std_lulus'] = $mjr_id[$index];
			// print_r ($data);
			$this->db->where('std_id', $value->std_id);
			$this->db->update('student', $data);
		}
	}
	public function isiStudentMajor()
	{
		$allStud = $this->Admin_model->getAllStud();		
		$getStdChoice = $this->Admin_model->getTypeMajor();		
		foreach ($allStud as $value) {
			foreach ($getStdChoice as $values) {
				$data['mt_id'] = $values->mt_id;
				$data['std_id'] = $value->std_id;
				$data['sm_value'] = rand(55,95);
				// print_r ($data);
				$this->db->insert('student_major', $data);
			}
			
		}
	}

}
