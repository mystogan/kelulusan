<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panlok extends CI_Controller {
	function __construct(){
		parent::__construct();
		SESSION_START();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Panlok_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		if ($this->session->userdata('log_user_id') == '') {
			$this->session->set_userdata('_err','Silahkan Login Dahulu');
			redirect(base_url());
		}

	}

	public function index()
	{
		redirect(base_url()."panlok/resume");

	}
	public function resume()
	{
		$id_college = $this->session->userdata('log_user_id');
		$data['kuota'] = $this->Panlok_model->getKuota($id_college);
		$data['choice'] = $this->Panlok_model->getSumChoice2($id_college);
		$data['sumStud'] = $this->Panlok_model->getSumStud($id_college);
		$data['graduate'] = $this->Panlok_model->getGraduate($id_college);
		$data['maxStud'] = $this->Panlok_model->getMaxStud($id_college);
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/resume',$data);
		$this->load->view('admin/footer',$data);
	}
	public function profil()
	{
		$id_college = $this->session->userdata('log_user_id');
		$data['college'] = $this->Panlok_model->getCollege($id_college);
		$data['position'] = $this->Panlok_model->getPosition();
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/profil',$data);
		$this->load->view('admin/footer',$data);
	}
	public function prodiPTN()
	{
		$id_college = $this->session->userdata('log_user_id');
		$data['prodiPTN'] = $this->Panlok_model->getMajoring($id_college);
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/prodiPTN',$data);
		$this->load->view('admin/footer',$data);
	}
	public function seleksi()
	{
		$this->load->model('Admin_model');
		$id_college = $this->session->userdata('log_user_id');
		$data['choice'] = $this->Admin_model->getChoice();
		$data['college'] = $this->Panlok_model->getCollege($id_college);
		$data['majoring'] = $this->Panlok_model->getMajoring($id_college);
		// print_r ($data['sumchoice']);
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/seleksi',$data);
		$this->load->view('admin/footer',$data);
	}
	public function getTblSeleksi($mjr_code = 0,$cs_id = 0) 
	{
		$this->load->model('Admin_model');
		$id_college = $this->session->userdata('log_user_id');
		$data['student'] = $this->Panlok_model->getStudent($mjr_code,$cs_id);
		$data['matpel'] = $this->Admin_model->getmatpel();
		$majoring = $this->Admin_model->getPerMajoring($mjr_code);
		$data['permajor'] = $majoring;
		$data['range'] = $this->Panlok_model->getPerMajorType($majoring->mt_id);
		$data['majoring'] = $this->Panlok_model->getMajoring($id_college);
		$data['college'] = $this->Panlok_model->getCollege($id_college);
		$data['countMajor'] = $this->Panlok_model->getCountMajor($mjr_code);
		// print_r ($data['majoring']);
		$data['mjr_code'] = $mjr_code;
		foreach ($data['student'] as $value) {
			$std_id = $value->std_id;
			$data['valueStudent'][$std_id] = $this->Panlok_model->getValueStudent($value->std_id, $value->mt_id);		
			$data['nilaiMatpel'][$std_id] = $this->Panlok_model->getMatpelStudent($std_id);		
		}
		$this->load->view('panlok/tbl_seleksi',$data);
	}
	public function getTblcalonlulusan($mjr_code = 0,$cs_id = 0) 
	{
		$this->load->model('Admin_model');
		$data['student'] = $this->Panlok_model->getMajorGraduate($mjr_code);
		$data['matpel'] = $this->Admin_model->getmatpel();
		$majoring = $this->Admin_model->getPerMajoring($mjr_code);
		$data['range'] = $this->Panlok_model->getPerMajorType($majoring->mt_id);
		// print_r ($data['range']);
		foreach ($data['student'] as $value) {
			$std_id = $value->std_id;
			$data['valueStudent'][$std_id] = $this->Panlok_model->getValueStudent($value->std_id, $value->mt_id);		
			$data['nilaiMatpel'][$std_id] = $this->Panlok_model->getMatpelStudent($std_id);		
		}
		$this->load->view('panlok/tbl_panlok',$data);
	}

	public function getDetailSiswa($std_id = 0)
	{
		$data['student'] = $this->Panlok_model->getPerStudent($std_id);
		$data['matpel'] = $this->Panlok_model->getPerStudentMatpel($std_id);
		$data['choice'] = $this->Panlok_model->getPerStudentChoice($std_id);
		if ($data['student']->std_lulus != '') {
			$data['majoring'] = $this->Panlok_model->getPerMajoring($data['student']->std_lulus);			
		}
		$this->load->view('panlok/modal_siswa',$data);
	}
	public function getDetailSiswa_afir($std_id = 0)
	{
		$data['student'] = $this->Panlok_model->getPerStudent($std_id);
		$data['matpel'] = $this->Panlok_model->getPerStudentMatpel($std_id);
		$data['choice'] = $this->Panlok_model->getPerStudentChoice($std_id);
		if ($data['student']->std_lulus != '') {
			$data['majoring'] = $this->Panlok_model->getPerMajoring($data['student']->std_lulus);			
		}
		$this->load->view('panlok/modal_siswa_afir',$data);
	}

	public function addVerifikator()
	{
		$veri_id = $this->input->post('veri_id');
		$data['pos_id'] = $this->input->post('pos_id');
		$data['coll_id'] = $this->input->post('coll_id');
		$data['veri_name'] = $this->input->post('veri_name');
		$data['veri_nip'] = $this->input->post('veri_nip');
		$data['veri_hp'] = $this->input->post('veri_hp');
		$data['veri_email'] = $this->input->post('veri_email');
		if($veri_id == '' || $veri_id == null){
			$data['veri_created'] = date('Y-m-d h:i:s');
			$this->db->insert('verifikator', $data);
		}else {
			$data['veri_updated'] = date('Y-m-d h:i:s');
			$this->db->where('veri_id', $veri_id);
			$this->db->update('verifikator', $data);
		}
		redirect(base_url()."panlok/profil");
	}

	public function cekkuotaubah()
	{
		$mjr_code = $this->input->post('mjr_id');
		$value = $this->input->post('value');
		$mjr = $this->Panlok_model->getPerMajoring($mjr_code);
		if($mjr->mjr_quota > $value ){
			$data['cek'] = 0;
			$data['pesan'] = 'Jumlah Perubahan lebih kecil dari Kuota';
			$data['kuota'] = $mjr->mjr_quota;
		}else {
			$simpan['mjr_quota_change'] = $value;
			$this->db->where('mjr_code', $mjr_code);
			$this->db->update('majoring', $simpan);
			$data['cek'] = 1;
			$data['pesan'] = 'Perubahan Berhasil';
		}
		echo json_encode($data);
		// echo $mjr_id." dan ".$value;
	}
	public function updatePassword()
	{
		$log_user_id = $this->session->userdata('log_user_id');
		$old_password = md5($this->input->post('old_password'));
		$new_password = md5($this->input->post('new_password'));

		$login = $this->Panlok_model->getLogin($log_user_id, $old_password);
		if($login->log_id == '' ){
			$data['cek'] = 0;
			$data['pesan'] = 'Password Salah Silahkan Ulangi Kembali';
		}else {
			$simpan['password'] = $new_password;
			$this->db->where('log_user_id', $log_user_id);
			$this->db->update('login_at', $simpan);

			$data['cek'] = 1;
			$data['pesan'] = 'Perubahan Berhasil';
		}
		echo json_encode($data);
		// echo $mjr_id." dan ".$value;
	}
	public function checkstudent($std_code = 0)
	{
		$std_code = trim($std_code);
		$student = $this->Panlok_model->getPerCodeStudent($std_code);
		$studentColl = $this->Panlok_model->getPerCodeCollStudent($std_code, $this->session->userdata('log_user_id'));
		if ($student->std_id == '') {
			$data['cek'] = 0;
			$data['std_id'] = 0;
			$data['pesan'] = 'Kode Siswa Tidak ada';
		}else if ($studentColl->std_id == '') {
			$data['cek'] = 0;
			$data['std_id'] = 0;
			$data['pesan'] = 'Siswa Ini tidak Memilih Program Studi di '.$this->session->userdata('name_college');
		}else {
			$data['cek'] = 1;
			$data['std_id'] = $student->std_id;
		}
		echo json_encode($data);
		// echo $mjr_id." dan ".$value;
	}
	public function updateChoiceStud()
	{
		$std_id = trim($this->input->post('std_id'));
		$mjr_code = $this->input->post('mjr_code');
		$status = $this->input->post('status');
		$step = $this->input->post('step');

		$id_college = $this->session->userdata('log_user_id');
		$college = $this->Panlok_model->getCollege($id_college);
		$result = $this->Panlok_model->cekkuotaMajor($mjr_code);
		if($college->coll_state == '-2'){
			$step = 3;
			$this->load->model('Admin_model');
			$hasil = $this->Admin_model->getSetting('ChangeKuota');
			// kondisi cek jika disetting adalah 1 maka kuota tidak nambah, jika 2 maka akan nambah
			if($hasil->status == 2){
				$this->db->set('mjr_quota_change', '(mjr_quota_change+1)',false);
				$this->db->where('mjr_code', $mjr_code);
				$this->db->update('majoring');
			}
		}

		// cek untuk kuota apakah masih ada atau tidak jika penuh langsung tidak diproses
		$result = $this->Panlok_model->cekkuotaMajor($mjr_code);
		$pilihan = $this->Panlok_model->cekpilihan($std_id,$mjr_code);
		if ($result->lulus < $result->mjr_quota_change || $status == 0) {
			// cek apakah siswa tersebut sudah masuk dalam perguruan tinggi yang lain atau belum
			$results = $this->Panlok_model->cekkuotaMajor($std_id);
			if($results->mjr_name == '' || $results->mjr_name == null){
				if ($status == 1) {
					$this->db->set('std_lulus', $mjr_code);
					$this->db->set('std_pilihan', $step.'|'.$pilihan);
					$this->db->where('std_id', $std_id);
					$this->db->update('student');
					if ($this->db->affected_rows() > 0) {
						$data['cek'] = 1;
						$data['pesan'] = 'Data Berhasil dirubah';
					}else {
						$data['cek'] = 0;
						$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
					}
				}else if ($status == 0){
					$this->db->set('std_lulus', null, TRUE);
					$this->db->set('std_pilihan', null, TRUE);
					$this->db->where('std_id', $std_id);
					$this->db->update('student');
					if ($this->db->affected_rows() > 0) {
						$data['cek'] = 1;
						$data['pesan'] = 'Data Berhasil dirubah';
					}else {
						$data['cek'] = 0;
						$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
					}
				}else {
					$data['cek'] = 0;
					$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
				}
			}else {
				$data['cek'] = 2;
				$data['pesan'] = 'Siswa Tersebut Sudah diterima di Instansi '.$results->mjr_name;
			}
			
		}else {
			$data['cek'] = 0;
			$data['pesan'] = 'Jumlah Lulus Sudah sama dengan kuota';
		}
		$data['id_siswa'] = $std_id;
		/**
		 * note
		 * 0 adalah status pesan yang error entah dari apa 
		 * 1 adalah pesan valid dan data bisa diubah 
		 * 2 adalah pesan ketika siswa itu sudah ada di PTN lain
		 */
		echo json_encode($data);
	}

	public function updateCadangStud()
	{
		$std_id = trim($this->input->post('std_id'));
		$mjr_code = $this->input->post('mjr_code');
		$status = $this->input->post('status');

		$id_college = $this->session->userdata('log_user_id');
		$college = $this->Panlok_model->getCollege($id_college);
		
		
		if ($status == 1) {
			$this->db->set('std_cadangan', $mjr_code);
			$this->db->where('std_id', $std_id);
			$this->db->update('student');
			if ($this->db->affected_rows() > 0) {
				$data['cek'] = 1;
				$data['pesan'] = 'Data Berhasil dirubah';
			}else {
				$data['cek'] = 0;
				$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
			}
		}else if ($status == 0){
			$this->db->set('std_cadangan', null, TRUE);
			$this->db->where('std_id', $std_id);
			$this->db->update('student');
			if ($this->db->affected_rows() > 0) {
				$data['cek'] = 1;
				$data['pesan'] = 'Data Berhasil dirubah';
			}else {
				$data['cek'] = 0;
				$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
			}
		}else {
			$data['cek'] = 0;
			$data['pesan'] = 'Terjadi kesalahan silahkan ulangi lagi';
		}
		
			
		
		$data['id_siswa'] = $std_id;
		/**
		 * note
		 * 0 adalah status pesan yang error entah dari apa 
		 * 1 adalah pesan valid dan data bisa diubah 
		 * 2 adalah pesan ketika siswa itu sudah ada di PTN lain
		 */
		echo json_encode($data);
	}

	
	public function calonlulusan()
	{
		$this->load->model('Admin_model');
		$id_college = $this->session->userdata('log_user_id');
		$data['majoring'] = $this->Panlok_model->getMajoring($id_college);
		$data['sumchoice'] = $this->Panlok_model->getSumChoice($id_college);
		$data['sumgraduate'] = $this->Panlok_model->getSumGraduate($id_college);
		// print_r ($data['sumchoice']);
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/calonlulusan',$data);
		$this->load->view('admin/footer',$data);
	}
	public function getKuota($mjr_code = 0)
	{
		$majoring = $this->Panlok_model->getPerMajoring($mjr_code);
		echo "Kuota ".$majoring->mjr_quota_change;
	}
	public function getInfoChoice($mjr_code = 0)
	{
		$this->load->model('Admin_model');
		$id_college = $this->session->userdata('log_user_id');
		$data['choice'] = $this->Admin_model->getChoice();
		$data['sumchoice'] = $this->Panlok_model->getSumChoice($mjr_code);
		$data['college'] = $this->Panlok_model->getCollege($id_college);
		$data['sumgraduate'] = $this->Panlok_model->getSumGraduate($mjr_code);
		$this->load->view('panlok/tabelinfoChoice',$data);

	}
	
	public function pengesahan()
	{
		$id_college = $this->session->userdata('log_user_id');
		$data['pengesahan'] = $this->Panlok_model->getPengesahan($id_college);
		$data['college'] = $this->Panlok_model->getCollege($id_college);
		$this->load->view('admin/header',$data);
		$this->load->view('panlok/pengesahan',$data);
		$this->load->view('admin/footer',$data);
	}

	public function pengesahanpanlok()
	{
		$id_college = $this->session->userdata('log_user_id');
		
		$data['coll_state'] = '-1';
		$data['coll_confirm'] = '1';
		$this->db->where('coll_id', $id_college);
		$this->db->update('college', $data);
		redirect(base_url()."panlok/pengesahan");
	}

	public function getMajorGraduate($mjr_code = 0)
	{
		$majorGraduate = $this->Panlok_model->getMajorGraduate($mjr_code);
		echo '<tr>
				<th>No</th>
				<th>Nomor</th> 
				<th>Nama</th>
				<th>L/P</th>
			</tr>';
		$i = 1;
		foreach ($majorGraduate as $value) {
			if ($value->std_gender == 0) {
				$jk = "L";
			}else {
				$jk = "P";
			}
			echo '<tr>
					<td>'.$i++.'</td>
					<td>'.$value->std_code.'</td> 
					<td>'.$value->std_name.'</td> 
					<td>'.$jk.'</td> 
				</tr>';
		}
	}
}
