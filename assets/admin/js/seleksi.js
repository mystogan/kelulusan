
$(document).ready(function() {
  loadTable()
});
function loadTable()
{
  let mjr_code = $('#mjr_code').val();
  let cs_id = $('#cs_id').val();
  $('#loadTable').load(base_url+"panlok/getTblSeleksi/"+mjr_code+"/"+cs_id);
  $('#kuotaprodi').load(base_url+"panlok/getKuota/"+mjr_code);
  $('#infoChoice').load(base_url+"panlok/getInfoChoice/"+mjr_code);
}

function detailsiswa(std_id)
{
  $('#detailsiswa').load(base_url+"panlok/getDetailSiswa/"+std_id);
}
function cekAll(mjr_code)
{
  let value = $('#id_student').val()
  let cek_all = JSON.parse(value);
  // console.log(cek_all)
  for (let index = 0; index < cek_all.length; index++) {
    let element = "#"+cek_all[index];
    $(element).prop('checked', true);
    ceksiswa(cek_all[index],mjr_code)
    console.log(element)
  }
}

function ceksiswa(std_id,mjr_code)
{
  if (document.getElementById(std_id).checked) {
      status = 1;
  } else {
      status = 0;
  }
  $.ajax({
    url : base_url+"panlok/updateChoiceStud", 
    data: {std_id:std_id, mjr_code:mjr_code, status:status, step:'2'}, 
    type: "post", 
    success:function(response){    
      let data = JSON.parse(response)
      if(data.cek == 1){
        //alert (data.pesan)
      }else if(data.cek == 2) {
        //alert (data.pesan)
      }else {
        //alert (data.pesan)
        document.getElementById(std_id).checked = false;
      }
      /**
		 * note
		 * 0 adalah status pesan yang error entah dari apa 
		 * 1 adalah pesan valid dan data bisa diubah 
		 * 2 adalah pesan ketika siswa itu sudah ada di PTN lain
		 */
      $('#infoChoice').load(base_url+"panlok/getInfoChoice/"+mjr_code);
      $('#kuotaprodi').load(base_url+"panlok/getKuota/"+mjr_code);

    },
    error: function(xhr, Status, err) {
      console.log("Terjadi error : "+Status)
    }
    
  });

}

function cekcadangan(std_id,mjr_code)
{
  if (document.getElementById("cadangan_"+std_id).checked) {
      status = 1;
  } else {
      status = 0;
  }
  $.ajax({
    url : base_url+"panlok/updateCadangStud", 
    data: {std_id:std_id, mjr_code:mjr_code, status:status}, 
    type: "post", 
    success:function(response){    
      let data = JSON.parse(response)
      if(data.cek == 1){
        //alert (data.pesan)
      }else if(data.cek == 2) {
        //alert (data.pesan)
      }else {
        //alert (data.pesan)
        document.getElementById("cadangan_"+std_id).checked = false;
      }

    },
    error: function(xhr, Status, err) {
      console.log("Terjadi error : "+Status)
    }
    
  });

}
function ceksiswa_afir(std_id,mjr_code)
{
  let idafir = 'afir'+mjr_code;
  status = 1;
  $.ajax({
    url : base_url+"panlok/updateChoiceStud", 
    data: {std_id:std_id, mjr_code:mjr_code, status:status, step:'3'}, 
    type: "post", 
    success:function(response){
      let data = JSON.parse(response)
      console.log(data)
      console.log(idafir)
      if(data.cek == 1){
        alert (data.pesan)
      }else if(data.cek == 2) {
        alert (data.pesan)
        document.getElementById(idafir).checked = false;
      }else {
        alert (data.pesan)
        document.getElementById(idafir).checked = false;
      }
      $('#kuotaprodi').load(base_url+"panlok/getKuota/"+mjr_code);
      // location.reload(); 
    },
    error: function(xhr, Status, err) {
      console.log("Terjadi error : "+Status)
    }
    
  });

}
// search dan muncul modal untuk selain afirmatif
$("#id_search").keyup(function(event) {
    if (event.keyCode === 13) {
      let std_code = $("#id_search").val();
      std_code = std_code.trim()
      $.ajax({
        url : base_url+"panlok/checkstudent/"+std_code, 
        type: "post", 
        success:function(response){    
          let data = JSON.parse(response)
          if(data.cek == 0){
            alert (data.pesan)
          }else {
            $('#detailsiswa').load(base_url+"panlok/getDetailSiswa/"+data.std_id);
            $("#viewSiswa").modal('show')
          }
        },
        error: function(xhr, Status, err) {
          console.log("Terjadi error : "+Status)
        }
        
      });
    }
});

// search dan muncul modal untuk afirmatif
$("#id_search_afirmatif").keyup(function(event) {
    if (event.keyCode === 13) {
      let std_code = $("#id_search_afirmatif").val();
      std_code = std_code.trim()
      $.ajax({
        url : base_url+"panlok/checkstudent/"+std_code, 
        type: "post", 
        success:function(response){    
          let data = JSON.parse(response)
          if(data.cek == 0){
            alert (data.pesan)
          }else {
            $('#detailsiswa').load(base_url+"panlok/getDetailSiswa_afir/"+data.std_id);
            $("#viewSiswa").modal('show')
          }
          
        },
        error: function(xhr, Status, err) {
          console.log("Terjadi error : "+Status)
        }
        
      });
    }
});