$(document).ready(function(){
  // load untuk rekap perprovinsi untuk login admin 
  setTimeout(function() {
    $('.loading').hide();
  }, 1000);
  $("#tahapan").load(base_url+"admin/getTabelTahapan");
  $.post(base_url+"admin/getCollegeIndividu" , { url: base_url+"admin/getCollegeIndividu" }, function(datas) {
    // document.getElementById('somediv').innerHTML = data;  
    // console.log(data);
    let data = JSON.parse(datas);
    for (let index = 0; index < data.length; index++) {
      let college = data[index];
      $.ajax({
        url : base_url+"admin/gettahapan",
        data: {college:college, no:index}, 
        type: "post", 
        success:function(result){          
          // console.log(result)
          // $("#tahapan").load(result);
          $('#tableTahapan tr:last').after(result);
        },
        error: function(xhr, Status, err) {
          console.log("Terjadi error : "+Status)
        }
        
      });
    }
  });
  $('#idDatatable').DataTable();
  move();
});

function tutupkelulusan(id,index,text)
{
    if (confirm("Apakah Anda akan yakin akan "+text+" ?")) {
      $.ajax({
        url : base_url+"admin/tutupkelulusan/"+id+"/"+index, 
        type: "post", 
        success:function(result){          
          alert(text+" berhasil diubah  ...");
          window.location.href = base_url+"admin/tahapan";
        },
        error: function(xhr, Status, err) {
          console.log("Terjadi error : "+Status)
        }
        
      });
    } else {
        return false;
    }
}
function autochoice(pilihan)
{
    if (confirm("Apakah Anda memilih Auto Pilihan "+pilihan)) {
      window.location.href = base_url+"admin/autoChoice/"+pilihan;
    }
}


