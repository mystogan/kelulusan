$(document).ready(function() { 
  // load untuk rekap perprovinsi untuk login admin 
  $('#idDatatable').DataTable();

    $.post(base_url+"admin/getprovince" , { url: base_url+"admin/getprovince" }, function(datas) {
      let data = JSON.parse(datas);
      for (let index = 0; index < data.length; index++) {
        let prov = data[index];
        $.ajax({
          url : base_url+"admin/getMaxProv",
          data: {prov:prov}, 
          type: "post", 
          success:function(result){          
            console.log(result)
            $('#rekapProv tr:last').after(result);
          },
          error: function(xhr, Status, err) {
            console.log("Terjadi error : "+Status)
          }
          
        });
      }
    });

});
